import React,{Component} from 'react';
import {StyleSheet, ScrollView, Text, View, ActivityIndicator,ProgressBarAndroid, SafeAreaView, TouchableOpacity,TouchableHighlight, FlatList} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Ionicons from 'react-native-vector-icons/Ionicons'; 
import Icon from 'react-native-vector-icons/MaterialIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import Feather from 'react-native-vector-icons/Feather';
import Fontisto from 'react-native-vector-icons/Fontisto';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'; 
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'; 
import { Input,Button,Header,Avatar,withBadge,Badge,Image   } from 'react-native-elements';
// import LinearGradient from 'react-native-linear-gradient';
import {LinearGradient} from 'expo-linear-gradient';
import DropDownPicker from 'react-native-dropdown-picker';
import Css from "../scripts/Design";
import {Fonts} from "../scripts/Fonts";

export default class trackOrder extends Component{
  constructor(props) {
      super(props);
      this.state = {
          username: 'Thierry',
          cart:null,
          quantityNeeded:1,
          loading:true,
      };
      this.cart_total=0;
  }

  componentDidMount() {
    AsyncStorage.getItem('@myCart').then((user_data_json) => {
        let cartData = JSON.parse(user_data_json);
        // alert(JSON.stringify(cartData));
        if(cartData != null) {
          // this.state.cart.push(cartData);
          this.setState({cart:cartData});
          this.setState({loading:false});
        }else{
          // alert('Object is null');
        }
    });
  }
  formatNumberWithCommas(x) {
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g , ",");
      // return x.replace(/B(?=(d{3})+(?!d))/g, ",");
  }

  render() {
    const GradientBtn = ({ name }) => (
       <LinearGradient colors={['#160e4e', '#556af9']} start={{ y: 0.5, x: 0 }} end={{ x: 1, y: 0.5 }}  style={styles.orderNowButton}>
          <Text style={styles.orderNowButtonText}>Order Now</Text>
          <FontAwesome name="long-arrow-right" color="#181532" size={21.5} />
       </LinearGradient>
    )
    const { state, goBack } = this.props.navigation;
    return (
      <View style={styles.container}>
          <View style={styles.settingmainHeader}>
              <TouchableOpacity style={styles.backIcon} onPress={() => this.props.navigation.goBack(null)}>
                  <MaterialIcons name="keyboard-arrow-left" size={30} color="white"/>
              </TouchableOpacity>
              <View>
                  <Text style={{fontSize: 15,fontFamily:Fonts.MontSerratSemiBold,color:'white'}}>Order Track</Text>
              </View>
              {/*<TouchableOpacity>
                  <Ionicons name="md-settings" color="white" size={16}/>
              </TouchableOpacity>*/}
          </View>
          <ScrollView style={styles.cartScrollView}>
              <View style ={styles.pageHeader}>
                <Text style ={styles.pageHeaderTitle}>
                    Track My Order
                </Text>
              </View>
              <View style={{height:10}}></View>
              <Text style ={styles.orderDescription}>Wed, 12 September</Text>
              <Text style ={styles.orderDescription}>Order ID: 5t36 - 9iu2 - 12i92</Text>

              <Text style ={styles.trackorderSubHeaderTitle}>Orders </Text>
              <View style={styles.trackOrderSection}>
                  <View style={styles.orderTrackView}>
                      <View style={styles.orderTrackStep}>
                          <View style={styles.trackConnector}>
                              <MaterialCommunityIcons name="record" size={30} color="lightgreen"/>
                              <MaterialCommunityIcons name="record" size={10} color="lightgreen"/>
                          </View>
                          <View>
                              <Image
                                  source={require('../assets/order/warehouse.png')}
                                  style={{ width: 60, height: 60,borderRadius:60,marginRight:2,marginLeft:5,}}
                                  PlaceholderContent={<ActivityIndicator />}
                              />
                          </View>
                          <View style={styles.orderStepDescr}>
                            <View>
                                <Text style={styles.orderStepDescrHeader}>Ready to Pickup</Text>
                                <Text style={styles.orderStepDescrSubHeader}>Order from EvolveShop</Text>
                            </View>
                            <View style={styles.orderStepTime}>
                                <Text style={styles.orderStepDescrHeader}>11:0</Text>
                            </View>
                          </View>
                      </View>
                      <View style={styles.orderConnector}>
                          <MaterialCommunityIcons name="record" size={10} color="lightgreen"/>
                          <MaterialCommunityIcons name="record" size={10} color="lightgreen"/>
                      </View>
                  </View>
                  <View style={styles.orderTrackView}>
                      <View style={styles.orderTrackStep}>
                          <View style={styles.trackConnector}>
                              <Ionicons name="md-checkmark-circle" size={35} color="lightgreen"/>
                              {/*<Ionicons name="md-checkmark-circle-outline" size={35} color="lightgreen"/>*/}
                              <MaterialCommunityIcons name="record" size={10} color="lightgreen"/>
                          </View>
                          <View>
                              <Image
                                  source={require('../assets/order/food-delivery.png')}
                                  style={{ width: 60, height: 60,borderRadius:60,marginRight:2,marginLeft:5,}}
                                  PlaceholderContent={<ActivityIndicator />}
                              />
                          </View>
                          <View style={styles.orderStepDescr}>
                            <View>
                                <Text style={styles.orderStepDescrHeader}>Order Bieng Derived</Text>
                                <Text style={styles.orderStepDescrSubHeader}>We are derivering to you</Text>
                            </View>
                            <View style={styles.orderStepTime}>
                                <Text style={styles.orderStepDescrHeader}>9:50</Text>
                            </View>
                          </View>
                      </View>
                      <View style={styles.orderConnector}>
                          <MaterialCommunityIcons name="record" size={10} color="lightgreen"/>
                          <MaterialCommunityIcons name="record" size={10} color="lightgreen"/>
                      </View>
                  </View>
                  <View style={styles.orderTrackView}>
                      <View style={styles.orderTrackStep}>
                          <View style={styles.trackConnector}>
                              <Ionicons name="md-checkmark-circle" size={35} color="lightgreen"/>
                              {/*<Ionicons name="md-checkmark-circle-outline" size={35} color="lightgreen"/>*/}
                              <MaterialCommunityIcons name="record" size={10} color="lightgreen"/>
                          </View>
                          <View>
                              <Image
                                  source={require('../assets/order/worker.png')}
                                  style={{ width: 60, height: 60,borderRadius:60,marginRight:2,marginLeft:5,}}
                                  PlaceholderContent={<ActivityIndicator />}
                              />
                          </View>
                          <View style={styles.orderStepDescr}>
                            <View>
                                <Text style={styles.orderStepDescrHeader}>Order Processed</Text>
                                <Text style={styles.orderStepDescrSubHeader}>we are preparing your order</Text>
                            </View>
                            <View style={styles.orderStepTime}>
                                <Text style={styles.orderStepDescrHeader}>9:50</Text>
                            </View>
                          </View>
                      </View>
                      <View style={styles.orderConnector}>
                          <MaterialCommunityIcons name="record" size={10} color="lightgreen"/>
                          <MaterialCommunityIcons name="record" size={10} color="lightgreen"/>
                      </View>
                  </View>
                  <View style={styles.orderTrackView}>
                      <View style={styles.orderTrackStep}>
                          <View style={styles.trackConnector}>
                              <Ionicons name="md-checkmark-circle" size={35} color="lightgreen"/>
                              {/*<Ionicons name="md-checkmark-circle-outline" size={35} color="lightgreen"/>*/}
                              <MaterialCommunityIcons name="record" size={10} color="lightgreen"/>
                          </View>
                          <View>
                              <Image
                                  source={require('../assets/order/checkout.png')}
                                  style={{ width: 60, height: 60,borderRadius:60,marginRight:2,marginLeft:5,}}
                                  PlaceholderContent={<ActivityIndicator />}
                              />
                          </View>
                          <View style={styles.orderStepDescr}>
                            <View>
                                <Text style={styles.orderStepDescrHeader}>Payment Confirmed</Text>
                                <Text style={styles.orderStepDescrSubHeader}>Awaiting Confirmation</Text>
                            </View>
                            <View style={styles.orderStepTime}>
                                <Text style={styles.orderStepDescrHeader}>8:20</Text>
                            </View>
                          </View>
                      </View>
                      <View style={styles.orderConnector}>
                          <MaterialCommunityIcons name="record" size={10} color="lightgreen"/>
                          <MaterialCommunityIcons name="record" size={10} color="lightgreen"/>
                      </View>
                  </View>
                  <View style={styles.orderTrackView}>
                      <View style={styles.orderTrackStep}>
                          <View style={styles.trackConnector}>
                              <MaterialCommunityIcons name="record" size={10} color="lightgreen"/>
                              <Ionicons name="md-checkmark-circle" size={35} color="lightgreen"/>
                              {/*<Ionicons name="md-checkmark-circle-outline" size={35} color="lightgreen"/>*/}
                          </View>
                          <View>
                              <Image
                                  source={require('../assets/order/order.png')}
                                  style={{ width: 60, height: 60,marginRight:2,marginLeft:5,}}
                                  PlaceholderContent={<ActivityIndicator />}
                              />
                          </View>
                          <View style={styles.orderStepDescr}>
                            <View>
                                <Text style={styles.orderStepDescrHeader}>Order Placed</Text>
                                <Text style={styles.orderStepDescrSubHeader}>We have received your order</Text>
                            </View>
                            <View style={styles.orderStepTime}>
                                <Text style={styles.orderStepDescrHeader}>8:00</Text>
                            </View>
                          </View>
                      </View>
                  </View>
              </View>

              <View style={styles.orderTrackDeliverAddress}>
                  <View style={styles.orderTrackDeliverAddressImage}>
                      <Image
                          source={require('../assets/order/real-estate-big.png')}
                          style={{ width: 60, height: 60,}}
                          PlaceholderContent={<ActivityIndicator />}
                      />
                  </View>
                  <View>
                      <Text style={styles.orderDeriverAddressTitle}>Delivery Address</Text>
                      <Text style={styles.orderDeriverAddressSubTitle}>Home, Work & Other Address</Text>
                      <Text style={styles.orderDeriverAddressSubTitleContent}>Road No: KK 4 Ave Gikondo</Text>
                  </View>
              </View>

          </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
      flex: 1,
      // paddingTop: 15,
      // backgroundColor: '#efeff4',
      backgroundColor: '#181532',
  },
  settingmainHeader:{
      width:'100%',
      flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center',
      padding:10,
      // backgroundColor:'#232148',
  },
  pageHeader:{
      flexDirection:'row',
      marginTop:0,
      paddingHorizontal:15,
      // justifyContent:'space-between',
  },
  pageHeaderTitle:{
      color:'white',
      fontSize:25,
      fontFamily:Fonts.MontSerratSemiBold,

  },
  orderDescription:{
      marginTop:1,
      marginLeft:'5%',
      color:'white',
      fontSize:15,
      color:'#eaeef0',
      fontFamily:Fonts.MontSerrat,

  },
  cartScrollView:{
      flex:1,
  },
  trackorderSubHeaderTitle:{
      color:'white',
      fontSize:20,
      color:'#eaeef0',
      fontFamily:Fonts.MontSerratSemiBold,
      marginLeft:20,
      paddingVertical:'5%',
      // backgroundColor:'red',
  },
  trackOrderSection:{
    marginLeft:'4%',
    // backgroundColor:'red',
    width:'94%',

  },
  orderTrackView:{
  },
  orderTrackStep:{
    flexDirection:'row',

  },
  orderConnector:{
    marginLeft:'2.7%',
    // alignItems:'center',
    // justifyContent:'center',
  },
  trackConnector:{
    alignItems:'center',
    justifyContent:'center',
  },
  orderStepDescr:{
    // backgroundColor:'blue',
    width:'72%',
    alignItems:'center',
    flexDirection:'row',
    justifyContent:'space-between',
  },
  orderStepDescrHeader:{
    fontFamily:Fonts.MontSerratSemiBold,
    color:'white',

  },
  orderStepDescrSubHeader:{
    fontFamily:Fonts.MontSerrat,
    color:'white',

  },
  orderStepTime:{
    right:0,
  },
  orderTrackDeliverAddressImage:{
    padding:20,
  },
  orderTrackDeliverAddress:{
    marginTop:30,
    width:'88%',
    marginLeft:'6%',
    backgroundColor:'lightgreen',
    textAlign:'center',
    marginBottom:50,
    borderRadius:10,
    flexDirection:'row',
    paddingVertical:10,
    alignItems:'center',
    justifyContent:'center',
  },
  orderDeriverAddressTitle:{
    fontFamily:Fonts.MontSerratSemiBold,
    fontSize:16,
  },
  orderDeriverAddressSubTitle:{
    fontFamily:Fonts.MontSerratSemiBold,
    fontSize:13,
  },
  orderDeriverAddressSubTitleContent:{
    fontFamily:Fonts.MontSerrat,
    fontSize:16,
  },
});
