// import React,{Component} from 'react';
// import { Button, View, Text } from 'react-native';
// import { NavigationContainer } from '@react-navigation/native';
// import { createStackNavigator } from '@react-navigation/stack';

// export default class homeScreen extends Component {
// 	render(){  
//   		const { navigation } = this.props;
// 	  	return (
// 	    	<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
// 	      		<Text>Welcome To Home Screen</Text>
// 	    	</View>
// 	  	);
// 	}
// }

import React,{Component} from 'react';
import {Platform, StyleSheet, Text, View,
  Animated,
  TouchableOpacity,
  Dimensions,
  ImageBackground,
  TextInput,
  Image,
  PixelRatio,
  PanResponder,
  ScrollView,
  TouchableHighlight,
  Alert,
  Modal,
    ProgressBarAndroid,
    ButtonGroup,
    NetInfo ,
  ActivityIndicator,
  StatusBar
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons'; 
import Icon from 'react-native-vector-icons/MaterialIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';  
import AsyncStorage from '@react-native-community/async-storage';
import {Fonts} from "../scripts/Fonts";
import products from './products';
import verification from './verification';
import profile from './profile';
import mycart from './mycart';
import mywishlist from './mywishlist';


export default class Home extends Component{
    constructor(props) {
        super(props);
        this.state = {
            message: '',
            userInfo:null,
        };
    }
  componentDidMount() {
      // AsyncStorage.getItem('userInfo').then((user_data_json) => {
      //     let userInfo = JSON.parse(user_data_json);
      //     if(userInfo == null) {
      //       this.props.navigation.navigate('schools',null);
      //     }
      //     else{
      //       // this.setState({userInfo:userInfo});
      //       // alert(JSON.stringify(userInfo));
      //     }
      // })

      // const { schoolName } = this.props.route.params;
      // const { phoneNumber } = this.props.route.params;
      // const { userInfo } = this.props.route.params;
      // alert(schoolName);
      // const {params} = this.props.navigation.state.route.params;
      // alert(JSON.stringify(schoolsdata));
  }
  render(){
    return (
        <Tab.Navigator 
          	keyboardDismissMode='on-drag'
          	shifting="true"
  			    barStyle={{ backgroundColor: '#181532', }}
  			    labeled="false"
      		// style={{ backgroundColor: 'green' }}
          	// tabBarOptions={{
           //  	activeColor: 'red',
           //  	activeTintColor: 'red',
           //  	inactiveTintColor :'#6d6c6c',
           //  	showIcon :true,
           //  	showLabel :false,
           //  	shifting :true,
          	// }}
        >
            <Tab.Screen 
                name="Products" 
                component={products} 
                options={{
                    tabBarLabel: <Text style={{ fontSize: 10,fontFamily:Fonts.MontSerratSemiBold, }}>Products</Text>,
                    tabBarIcon: ({ color, size }) => (
                      <MaterialCommunityIcons name="food-variant" color="white" size={23} />
                    ),
                    navigationOptions: ({navigation, route }) => ({
                      userInfo: this.state.userInfo,
                    }),
                	// tabBarColor:'red',
                }}
            />
            <Tab.Screen 
                name="Cart" 
                component={mycart} 
                options={{
                    tabBarLabel: <Text style={{ fontSize: 10,fontFamily:Fonts.MontSerratSemiBold, }}>Cart</Text>,
                    tabBarIcon: ({ color, size }) => (
                      <Feather name="shopping-cart" color="white" size={21.5} />
                    ),
                }}
            />
            <Tab.Screen 
                name="Wish List" 
                component={mywishlist} 
                options={{
                    tabBarLabel: <Text style={{ fontSize: 10,fontFamily:Fonts.MontSerratSemiBold, }}>Wish List</Text>,
                    tabBarIcon: ({ color, size }) => (
                      <FontAwesome name="shopping-basket" color="white" size={21.5} />
                    ),
                    navigationOptions: ({navigation, route }) => ({
                      // headerShown: false
                    })
                }}
            />
            <Tab.Screen 
                name="Profile" 
                component={profile} 
                options={{
                    tabBarLabel: <Text style={{ fontSize: 10,fontFamily:Fonts.MontSerratSemiBold, }}>Profile</Text>,
                    tabBarIcon: ({ color, size }) => (
                      <FontAwesome name="user-circle" color="white" size={21.5} />
                    ),
                    navigationOptions: ({navigation, route }) => ({
                      // headerShown: false
                    })
                }}
            />
        </Tab.Navigator>
    );
  }
}
const Tab = createMaterialBottomTabNavigator();
