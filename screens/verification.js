import React,{Component} from 'react';
import {StyleSheet, ScrollView, Text, View, Image, ActivityIndicator, SafeAreaView, TouchableOpacity,TouchableHighlight} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Ionicons from 'react-native-vector-icons/Ionicons'; 
import Icon from 'react-native-vector-icons/MaterialIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'; 
import AppIntroSlider from 'react-native-app-intro-slider';
import {iOSUIKit, systemWeights, sanFranciscoWeights, robotoWeights,human,material } from 'react-native-typography';
import { Input,Button  } from 'react-native-elements';
// import LinearGradient from 'react-native-linear-gradient';
import {LinearGradient} from 'expo-linear-gradient';
import OTPInputView from '@twotalltotems/react-native-otp-input'
import Css from "../scripts/Design";
import {Fonts} from "../scripts/Fonts";
export default class Login extends Component{
	render(){
		return(
			<ScrollView style={styles.scrollView}>
				<View style={styles.mainContainer}>
					<View style={styles.loginSection}>
            			<Image source={require('../assets/Baby-Mobile.png')} style={styles.loginImage}/>
            			<Text style={styles.loginHeaderText}>Verification</Text>
            			<Text style={styles.loginText}>We texted you a code to verify your phone number</Text>
            			<OTPInputView 
							style={{width: '80%', height: '15%',marginTop:'10%',}}
							pinCount={4} 
							autoFocusOnLoad
						    codeInputFieldStyle={styles.underlineStyleBase}
						    codeInputHighlightStyle={styles.underlineStyleHighLighted}
						    onCodeFilled = {(code => {
						        console.log(`Code is ${code}, you are good to go!`)
						    })}
						/>
						<TouchableOpacity style={styles.resendSection}><Text style={styles.resendText}>Resend</Text></TouchableOpacity>
						<Button
  							icon={{name: 'arrow-right', type: 'font-awesome' ,color:'white',size:15,style: styles.authenticationInnerIcon}}
						  	// raised
						  	title="Continue"
						  	containerStyle={{height:'9.5%',background:'green',marginBottom:'20%'}}
							iconContainerStyle={{position: 'absolute', right: 10}}
						  	TouchableComponent={TouchableOpacity}
						  	ViewComponent={LinearGradient} // Don't forget this!
						  	linearGradientProps={{
							    colors: ['#160e4e', '#556af9'],
							    start: { x: 0, y: 0.5 },
							    end: { x: 1, y: 0.5 },
							}}
							onPress={()=>this.props.navigation.navigate('userInfo',null)}
						  	buttonStyle={styles.authenticationButton}
						  	textStyle={{fontFamily: Fonts.MontSerrat}}
						  	titleStyle={{fontFamily: Fonts.MontSerrat}}
						/>
	            	</View>
				</View>
			</ScrollView>
		)
	}
}
const styles = StyleSheet.create({
	scrollView:{
        flex: 1,
        height: "100%",
        backgroundColor: '#181532',
        paddingBottom:200,
	},
	mainContainer:{
        flex: 1,
        // backgroundColor: 'red',
        opacity: 1,
		alignItems:'center',

	},
	loginSection:{
		// backgroundColor:'red',
		marginTop:'20%',
		width:'90%',
		height:'100%',
		alignItems:'center',

	},
	loginImage:{
		width:100,
		height:100,
	},
	loginHeaderText:{
		marginTop:'3%',
		fontFamily:Fonts.MontSerratSemiBold,
		fontSize:17,
		textAlign:'center',
		color:'white',
	},
	loginText:{
		marginTop:'0%',
		fontFamily:Fonts.MontSerrat,
		fontSize:15,
		textAlign:'center',
		color:'white',
	},

	//OTC STYLE
	borderStyleBase: {
	    width: 30,
	    height: 45
	},

	  borderStyleHighLighted: {
	    borderColor: "#03DAC6",
	},

	underlineStyleBase: {
	    width: 40,
	    height: 45,
	    borderWidth: 2,
	    // borderBottomWidth: 2,
	},

	underlineStyleHighLighted: {
	    borderColor: "#03DAC6",
	},
	// OTC STYLE

	resendSection:{
		marginTop:'10%',
		marginBottom:'8%',
	},
	resendText:{
		fontFamily:Fonts.MontSerrat,
		fontSize:15,
		textAlign:'center',
		color:'white',
	},
	authenticationButton:{
		borderRadius:50,
		paddingHorizontal:'32%',
		paddingVertical:'5%',
		fontFamily:Fonts.MontSerrat,
		// backgroundColor:'#160e4e',
	},
	authenticationInnerIcon:{
		backgroundColor:'#3954bb',
		padding:4,
		borderRadius:40,
	},
})