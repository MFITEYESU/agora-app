import React,{Component,useState } from 'react';
import {PermissionsAndroid,Switch,StyleSheet, ScrollView, Text, View, ActivityIndicator, SafeAreaView, TouchableOpacity,TouchableHighlight, FlatList} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Ionicons from 'react-native-vector-icons/Ionicons'; 
import Icon from 'react-native-vector-icons/MaterialIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import Feather from 'react-native-vector-icons/Feather';
import Fontisto from 'react-native-vector-icons/Fontisto';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'; 
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'; 
import { Input,Button,Header,Avatar,withBadge,Badge,Image   } from 'react-native-elements';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
// import Geolocation  from '@react-native-community/geolocation';
import LinearGradient from 'react-native-linear-gradient';
// import {LinearGradient} from 'expo-linear-gradient';
import DropDownPicker from 'react-native-dropdown-picker';
import Css from "../scripts/Design";
import {Fonts} from "../scripts/Fonts";

export default class derivery extends Component{
  constructor(props) {
      super(props);
      this.state = {
          selectedCard:2,
          country: 'VISA CARD',
          switch1Value: false,
          locationMethod:2,
      };
  }
  changeLocationMethod(method){
    this.setState({locationMethod:method});

  }
  toggleSwitch1 = (value) => {
      this.setState({switch1Value: value})
      // console.log('Switch 1 is: ' + value)
  }
  render() {
    const { state, goBack } = this.props.navigation;
    return (
      <View style={styles.container}>
          <View style={styles.settingmainHeader}>
              <TouchableOpacity style={styles.backIcon} onPress={() => this.props.navigation.goBack(null)}>
                  <MaterialIcons name="keyboard-arrow-left" size={30} color="white"/>
              </TouchableOpacity>
              <View>
                  <Text style={{fontSize: 15,fontFamily:Fonts.MontSerratSemiBold,color:'white'}}>Delivery Address</Text>
              </View>
              {/*<TouchableOpacity>
                  <Ionicons name="md-settings" color="white" size={16}/>
              </TouchableOpacity>*/}
          </View>
          <ScrollView>
              <View style ={styles.pageHeader}>
                <Text style ={styles.pageHeaderTitle}>
                    Where are your ordered items derivered ?
                </Text>
                <TouchableOpacity style = {styles.usernameInputContainer}>
                    {/*<Text style={styles.addmore}>ADD MORE +</Text>*/}
                </TouchableOpacity>
              </View>
              <View style={{height:15}}></View>

              <View style={{height:15}}></View>
              <Text style ={styles.paymentMethodTitle}>Locating Methods</Text>
              <View style={styles.howToGetLocation}>
                <TouchableOpacity style={[styles.locationWay,{backgroundColor:this.state.locationMethod == 1?('orange'):('white')}]} onPress={(method)=>this.changeLocationMethod(1)}>
                  <View style={styles.locationData}>
                      <View>
                          <Text style={{fontSize:15,fontFamily:Fonts.MontSerratSemiBold,textAlign:'center'}}>Use Google Map</Text>
                      </View>
                      <View style={styles.cardTitleLeft}>  
                          <MaterialCommunityIcons name="google-maps" color="#181532" size={30}/>
                      </View>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity style={[styles.locationWay,{backgroundColor:this.state.locationMethod == 2?('orange'):('white')}]} onPress={(method)=>this.changeLocationMethod(2)}>
                  <View style={styles.locationData}>
                      <View>
                          <Text style={{fontSize:15,fontFamily:Fonts.MontSerratSemiBold,textAlign:'center'}}>Custom Location</Text>
                      </View>
                      <View style={styles.cardTitleLeft}>  
                          <Entypo name="address" color="#181532" size={30}/>
                      </View>
                  </View>
                </TouchableOpacity>
              </View>
              {
                this.state.locationMethod == 1?(
                  <View style={styles.addLocationSection}>
                      <View style={styles.addCardForm}>
                          <View style={styles.cardInfo}>
                              <GooglePlacesAutocomplete
                                placeholder='Search'
                                fetchDetails={true}
                                autoFocus={false}
                                returnKeyType={'default'}
                                styles={{
                                  textInputContainer: {
                                    backgroundColor: 'rgba(0,0,0,0)',
                                    borderTopWidth: 0,
                                    borderBottomWidth: 0,
                                  },
                                  textInput: {
                                    marginLeft: 0,
                                    marginRight: 0,
                                    height: 38,
                                    color: '#5d5d5d',
                                    fontSize: 16,
                                  },
                                  predefinedPlacesDescription: {
                                    color: '#1faadb',
                                  },
                                }}
                                onPress={(data, details = null) => {
                                  // 'details' is provided when fetchDetails = true
                                  console.log(data, details);
                                }}
                                query={{
                                  key: 'AIzaSyC-GmJ5Ktp93pTvvZKJF7AymfL292hJ13s',
                                  language: 'en',
                                }}
                                currentLocation={true}
                                currentLocationLabel='Current location'
                              />
                          </View>

                          <View style={styles.saveCardSection}>
                              <Text style={styles.saveCard}>Save location for future delivery</Text>
                              <Switch
                                trackColor={{ false: "#767577", true: "#81b0ff" }}
                                styles={styles.switchView}
                                onValueChange = {this.toggleSwitch1}
                                value = {this.state.switch1Value}
                              />
                          </View>
                        <TouchableOpacity style={styles.locationProceedPayment} onPress={()=>this.props.navigation.navigate('placeOrder',null)}>
                              <Text style={styles.addPaymentText}>Proceed to payment</Text>
                              <FontAwesome name="long-arrow-right" size={30} color='white' style={{textAlign:'center'}}/>
                          </TouchableOpacity>

                      </View>
                  </View>
                ):(
                  <View style={styles.addLocationSection}>
                    <View style={styles.addCardForm}>
                        <Input
                          label="DISTRICT"
                          containerStyle={{height:'70%'}}
                          labelStyle={{fontSize:10,fontFamily:Fonts.MontSerrat,color:'gold'}}
                          placeholder='Eg. KICUKIRO'
                          inputStyle={{fontFamily:Fonts.MontSerrat,color:'white',fontSize:15}}
                          placeholderTextColor="white"
                          leftIcon={
                            <Entypo
                              name='location-pin'
                              size={20}
                              color='gold'
                            />
                          }
                        />
                        <Input
                          label="SECTOR"
                          containerStyle={{height:'70%'}}
                          labelStyle={{fontSize:10,fontFamily:Fonts.MontSerrat,color:'gold'}}
                          placeholder='Eg. GIKONDO'
                          inputStyle={{fontFamily:Fonts.MontSerrat,color:'white',fontSize:15}}
                          placeholderTextColor="white"
                          leftIcon={
                            <Entypo
                              name='location-pin'
                              size={20}
                              color='gold'
                            />
                          }
                        />
                        <Input
                          label="CELL"
                          containerStyle={{height:'70%'}}
                          labelStyle={{fontSize:10,fontFamily:Fonts.MontSerrat,color:'gold'}}
                          placeholder='Eg. GIKONDO'
                          inputStyle={{fontFamily:Fonts.MontSerrat,color:'white',fontSize:15}}
                          placeholderTextColor="white"
                          leftIcon={
                            <Entypo
                              name='location-pin'
                              size={20}
                              color='gold'
                            />
                          }
                        />
                        <Input
                          label="VILLAGE"
                          containerStyle={{height:'70%'}}
                          labelStyle={{fontSize:10,fontFamily:Fonts.MontSerrat,color:'gold'}}
                          placeholder='Eg. SEJEMU'
                          inputStyle={{fontFamily:Fonts.MontSerrat,color:'white',fontSize:15}}
                          placeholderTextColor="white"
                          leftIcon={
                            <Entypo
                              name='location-pin'
                              size={20}
                              color='gold'
                            />
                          }
                        />
                        <Input
                          label="MAIN STREET"
                          containerStyle={{height:'70%'}}
                          labelStyle={{fontSize:10,fontFamily:Fonts.MontSerrat,color:'gold'}}
                          placeholder='Eg. KK 4 Ave'
                          inputStyle={{fontFamily:Fonts.MontSerrat,color:'white',fontSize:15}}
                          placeholderTextColor="white"
                          leftIcon={
                            <Entypo
                              name='location-pin'
                              size={20}
                              color='gold'
                            />
                          }
                        />
                        <View style={styles.saveCardSection}>
                            <Text style={styles.saveCard}>Save location for future delivery</Text>
                            <Switch
                              trackColor={{ false: "#767577", true: "#81b0ff" }}
                              styles={styles.switchView}
                              onValueChange = {this.toggleSwitch1}
                              value = {this.state.switch1Value}
                            />
                        </View>
                        <TouchableOpacity style={styles.locationProceedPayment} onPress={()=>this.props.navigation.navigate('placeOrder',null)}>
                            <Text style={styles.addPaymentText}>Proceed to payment</Text>
                            <FontAwesome name="long-arrow-right" size={30} color='white' style={{textAlign:'center'}}/>
                        </TouchableOpacity>
                        
                    </View>
                  </View>
                )
              }

          </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
      flex: 1,
      // paddingTop: 15,
      // backgroundColor: '#efeff4',
      backgroundColor: '#181532',
  },
  pageHeader:{
      flexDirection:'row',
      marginTop:0,
      paddingHorizontal:15,
      // justifyContent:'space-between',
  },
  pageHeaderTitle:{
      color:'white',
      fontSize:25,
      fontFamily:Fonts.MontSerratSemiBold,

  },
  settingmainHeader:{
      width:'100%',
      flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center',
      padding:10,
      // backgroundColor:'#232148',
  },
  paymentTotal:{
      // flexDirection:'row',
      marginTop:'4%',
      paddingHorizontal:15,
      // justifyContent:'space-between',
  },
  paymentTotalTitle:{
      color:'white',
      fontSize:15,
      fontFamily:Fonts.MontSerrat,

  },
  paymentMethodTitle:{
      color:'white',
      fontSize:15,
      fontFamily:Fonts.MontSerrat,
      paddingHorizontal:15,
      marginBottom:'1%',

  },
  paymentTotalSubTitle:{

      color:'white',
      fontSize:27,
      fontFamily:Fonts.MontSerratSemiBold,
  },
  usernameInputContainer:{
      marginLeft:'5%',
      justifyContent:'center',
  },
  addmore:{
      fontFamily:Fonts.MontSerratSemiBold,
      color:'#531de3',
  },
  howToGetLocation:{
    flexDirection:'row',
  },
  locationWay:{
      backgroundColor:'#ffffff',
      marginLeft:10,
      borderRadius:10,
      paddingVertical:3,
      paddingHorizontal:3,
      width:'46%',
  },
  locationData:{
      // flexDirection:'row',
      justifyContent:'space-between',
      // padding:10,
  },
  cardTitleLeft:{
      alignItems:'center',
      justifyContent:'center',
  },
  addLocationSection:{
      alignItems:'center',
      marginTop:'5%',
      marginBottom:380,
  },
  addCardForm:{
      // backgroundColor:'green',
      width:'80%',
      height:100,
  },
  cardInfo:{
    flexDirection:'row'
  },

  locationProceedPayment:{
      borderRadius:50,
      paddingHorizontal:'10%',
      paddingVertical:'3%',
      fontFamily:Fonts.MontSerrat,
      backgroundColor:'#160e4e',
      borderWidth:1.5,
      borderColor:'white',
      flexDirection:'row',
      alignItems:'center',
      justifyContent:'space-between',
  },
  addPaymentText:{
      color:'white',
      fontFamily:Fonts.MontSerratSemiBold,
      fontSize:15,
  },
  saveCardSection:{
    flexDirection:'row',
    marginLeft:-10,
    // backgroundColor:'red',
  },
  saveCard:{
      marginTop:15,
      marginRight:'4%',
      marginBottom:20,
      color:'white',
      fontFamily:Fonts.MontSerratSemiBold,
  },
  switchView:{
    marginTop:-5,
    backgroundColor:'blue',
  }
});
