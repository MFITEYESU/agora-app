import React,{Component} from 'react';
import {StyleSheet, ScrollView, Text, View, Image, ActivityIndicator, SafeAreaView, TouchableOpacity,TouchableHighlight} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Ionicons from 'react-native-vector-icons/Ionicons'; 
import Icon from 'react-native-vector-icons/MaterialIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'; 
import AppIntroSlider from 'react-native-app-intro-slider';
import {iOSUIKit, systemWeights, sanFranciscoWeights, robotoWeights,human,material } from 'react-native-typography';
import { Input,Button  } from 'react-native-elements';
// import LinearGradient from 'react-native-linear-gradient';
import {LinearGradient} from 'expo-linear-gradient';
import Css from "../scripts/Design";
import {Fonts} from "../scripts/Fonts";
export default class Login extends Component{
	render(){
		return(
			<ScrollView style={styles.scrollView}>
				<View style={styles.mainContainer}>
					<View style={styles.loginSection}>
	            			<Image source={require('../assets/Baby-Mobile.png')} style={styles.loginImage}/>
	            			<Text style={styles.loginHeaderText}>Let's Get Started</Text>
	            			<Text style={styles.loginText}>Enter your mobile phone to enable 2-steps verfication</Text>
	            			<Input
							  	placeholder='Phone Number'
							  	leftIcon={
							    	<Icon
							      		name='phone'
								      		size={24}
								      		color='white'
								    	/>
							  	}
							  	inputContainerStyle={styles.phoneInput}
							  	inputStyle={{fontFamily:Fonts.MontSerrat,color:'white'}}
							  	placeholderTextColor="white"
							/>

							<Button
	  							icon={{name: 'arrow-right', type: 'font-awesome' ,color:'white',size:15,style: styles.authenticationInnerIcon}}
							  	// raised
							  	title="Continue"
							  	containerStyle={{height:'9.5%',marginBottom:'20%'}}
								iconContainerStyle={{position: 'absolute', right: 10}}
							  	TouchableComponent={TouchableOpacity}
							  	ViewComponent={LinearGradient} // Don't forget this!
							  	linearGradientProps={{
								    colors: ['#160e4e', '#556af9'],
								    start: { x: 0, y: 0.5 },
								    end: { x: 1, y: 0.5 },
								}}
								onPress={()=>this.props.navigation.navigate('verification',null)}
							  	buttonStyle={styles.authenticationButton}
							  	textStyle={{fontFamily: Fonts.MontSerrat}}
							  	titleStyle={{fontFamily: Fonts.MontSerrat}}
							/>

					</View>
				</View>
			</ScrollView>
		)
	}
}
const styles = StyleSheet.create({
	scrollView:{
        flex: 1,
        height: "100%",
        backgroundColor: '#181532',
        paddingBottom:200,
	},
	mainContainer:{
        flex: 1,
        backgroundColor: '#181532',
        opacity: 1,
		alignItems:'center',

	},
	loginSection:{
		// backgroundColor:'red',
		marginTop:'20%',
		width:'90%',
		height:'100%',
		alignItems:'center',

	},
	loginImage:{
		width:100,
		height:100,
	},
	loginHeaderText:{
		marginTop:'3%',
		fontFamily:Fonts.MontSerratSemiBold,
		fontSize:17,
		textAlign:'center',
		color:'white',
	},
	loginText:{
		marginTop:'1%',
		fontFamily:Fonts.MontSerrat,
		fontSize:15,
		textAlign:'center',
		color:'white',
	},
	phoneInput:{
		marginTop:'20%',
		paddingHorizontal:10,
		// borderColor:'#d9d9d6',
		borderColor:'#232149',
		backgroundColor:'#232149',
		borderWidth:1,
		borderRadius:5,
		width:'90%',
		marginLeft:'5%',
		color:'white',

	},
	authenticationButtonContainer:{
		// backgroundColor:,
		height:'50%',
	},
	authenticationButton:{
		borderRadius:50,
		paddingHorizontal:'32%',
		paddingVertical:'5%',
		fontFamily:Fonts.MontSerrat,
		// backgroundColor:'#160e4e',
	},
	authenticationInnerIcon:{
		backgroundColor:'#3954bb',
		padding:4,
		borderRadius:40,
	}
})