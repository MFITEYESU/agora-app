import React,{Component} from 'react';
import {StyleSheet, ScrollView, Text, View, ActivityIndicator, SafeAreaView, TouchableOpacity,TouchableHighlight, FlatList} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Ionicons from 'react-native-vector-icons/Ionicons'; 
import Icon from 'react-native-vector-icons/MaterialIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import Feather from 'react-native-vector-icons/Feather';
import Fontisto from 'react-native-vector-icons/Fontisto';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'; 
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'; 
import { Input,Button,Header,Avatar,withBadge,Badge,Image   } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import DropDownPicker from 'react-native-dropdown-picker';
import {Collapse, CollapseHeader, CollapseBody} from "accordion-collapse-react-native";
import Animated from "react-native-reanimated";
import Css from "../scripts/Design";
import {Fonts} from "../scripts/Fonts";

export default class productDetail extends Component{
  constructor(props) {
      super(props);
      const { productName } = this.props.route.params;
      const { productCategorie } = this.props.route.params;
      const { productImage } = this.props.route.params;
      const { productId } = this.props.route.params;
      const { productQuantity } = this.props.route.params;
      const { productStockQuantity } = this.props.route.params;
      const { productPrice } = this.props.route.params;
      const { productDescription } = this.props.route.params;
      this.state = {
        productName:productName,
        productCategorie:productCategorie,
        productImage:productImage,
        productId:productId,
        productQuantity:productQuantity,
        productStockQuantity:productStockQuantity,
        productPrice:productPrice,
        productDescription:productDescription,
        relatedProducts:[
          { productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/coca.jpg'), productId: 1,productQuantity:1,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
          { productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/cocacola.jpg'), productId: 2,productQuantity:1,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
          { productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/banana.jpg'), productId: 3,productQuantity:1,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
          { productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/cocacola.jpg'), productId: 4,productQuantity:1,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
          { productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/coca.jpg'), productId: 5,productQuantity:1,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
          { productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/banana.jpg'), productId: 6,productQuantity:1,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
          { productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/cocacola.jpg'), productId: 7,productQuantity:1,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
          { productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/banana.jpg'), productId: 8,productQuantity:1,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
        ],
        collapsed:false,
        headerBackground:'white',
        quantityNeeded:1,
        cart:[],
        wishList:[],
        loading:false,
      };
  }
  handleScroll(event: Object) {
   // console.log(event.nativeEvent.contentOffset.y);
   // alert(event.nativeEvent.contentOffset.y)
     if (event.nativeEvent.contentOffset.y > 340) {
          this.setState({headerBackground:'#181532'});
     }else{
          this.setState({headerBackground:'white'});
     }
  }
  changeQuantity(event){
    let quantityNeeded = this.state.quantityNeeded;
    if (event == 'increase') {
      this.setState({quantityNeeded:quantityNeeded+1})
    }else{
      this.setState({quantityNeeded:quantityNeeded-1})

    }
  }
  componentDidMount(){
      AsyncStorage.multiGet(['@myCart','@myWishList',]).then((user_data_json) => {
          let cartData=user_data_json[0][1];
          let wishlistData=user_data_json[1][1];
          if (JSON.parse(cartData) != null) {
            // this.state.cart.push(cartData);
              this.setState({cart:JSON.parse(cartData)});
              this.setState({loading:false});

          }
          if (JSON.parse(wishlistData) != null) {
              // alert(wishlistData);
            // this.state.cart.push(cartData);
              this.setState({wishList:JSON.parse(wishlistData)});
              this.setState({loading:false});

          }
      });
  }
  moveToWishlist(item){
      // alert(JSON.stringify(this.state.wishList));
      if (this.state.wishList.length > 0) {
          let Index=this.state.wishList.findIndex(obj => obj.productId === item.productId);
          // alert(Index);
          if(Index < 0){
            this.state.wishList.push(item);
            let mywishList=JSON.stringify(this.state.wishList);
            // alert(myCart);
            try {
              AsyncStorage.setItem('@myWishList', mywishList);
            } catch(e) {}
            alert('Product Has Been Added To WishList')
            
          }else{
            // alert('The Product Exists in Cart')
          }
      }
      else{
          this.state.wishList.push(item);
          let mywishList=JSON.stringify(this.state.wishList);
          try {
            AsyncStorage.setItem('@myWishList', mywishList);
          } catch(e) {}
          alert('Product Has Been Added To WishList')
      }
  }

  deleteproductFromCart(productId,event,index){
    this.state.cart.splice(index,1);
    this.setState({cart:this.state.cart});
    AsyncStorage.setItem('@myCart', JSON.stringify(this.state.cart));
  }

  addToCart(item){
    // alert(JSON.stringify(this.state.cart));
    if (this.state.cart.length > 0) {
      let Index=this.state.cart.findIndex(obj => obj.productId === item.productId);
      // alert(Index);
      if(Index < 0){
        this.state.cart.push(item);
        let myCart=JSON.stringify(this.state.cart);
        // alert(myCart);
        try {
          AsyncStorage.setItem('@myCart', myCart);
        } catch(e) {}
        alert('Product Has Been Added To Cart')
        
      }else{
        // alert('The Product Exists in Cart')
      }
    }
    else{ 
      let product=[item];
      this.state.cart.push(item);
      let myCart=JSON.stringify(product);
      try {
        AsyncStorage.setItem('@myCart', myCart);
      } catch(e) {}
      alert('Product Has Been Added To Cart')
    }  
  }
  render() {
    const productInfo={
        productName: this.state.productName,
        productCategorie:this.state.productCategorie,
        productImage:this.state.productImage,
        productId: this.state.productId,
        productStockQuantity:this.state.productQuantity,
        productQuantity:1,
        productPrice:this.state.productPrice,
        productDescription:this.state.productDescription,
    }
    const { state, goBack } = this.props.navigation;
    return (
      <View style={styles.container}>
          <View style={[styles.mainHeader,{backgroundColor:this.state.headerBackground,}]}>
              <TouchableOpacity style={styles.backIcon} onPress={() => this.props.navigation.goBack(null)}>
                  <MaterialIcons name="keyboard-arrow-left" size={30} color={this.state.headerBackground == 'white'?('black'):('white')}/>
              </TouchableOpacity>
              <TouchableOpacity>
                  <MaterialCommunityIcons name="share-variant" size={20} color={this.state.headerBackground == 'white'?('black'):('white')}/>
              </TouchableOpacity>
              {/*<TouchableOpacity>
                  <Ionicons name="md-settings" color="white" size={16}/>
              </TouchableOpacity>*/}
          </View>
          <ScrollView 
            onScroll={this.handleScroll.bind(this)}
          >
              <View style={styles.productcard}>
                  <View style={styles.productcardImageSection}>
                      <View style={styles.productimageContainer}>
                          <Image
                              source={require('../assets/products/cocacola.jpg')}
                              style={{ width: 200, height: 200,borderRadius:10,}}
                              PlaceholderContent={<ActivityIndicator />}
                          />
                      </View>
                      <TouchableOpacity style={styles.productLike} onPress={(item)=>this.moveToWishlist(productInfo)}>  
                          <AntDesign name="hearto" color="gray" size={20}/>
                      </TouchableOpacity>
                  </View>
                  <View style={styles.productClassifications}>
                      <Text style={styles.subcategorie}>Fresh Frinks</Text>
                      <Text style={styles.categorie}>MILKANA</Text>
                      <Text style={styles.productName}>{this.state.productName}</Text>
                      <View style={styles.cartInfo}>
                          <Text style={styles.stockStatus}>Status in stock</Text>

                          <View style={styles.productNumber}>
                              <TouchableOpacity style={styles.quantityReduce} onPress={()=>this.changeQuantity('decrease')}>
                                <Entypo name="minus" size={21} color="black"/>
                              </TouchableOpacity>
                              <Text style={styles.quantityReached}>{this.state.quantityNeeded}</Text>
                              <TouchableOpacity style={styles.quantityIncrease} onPress={()=>this.changeQuantity('increase')}>
                                <Entypo name="plus" size={21} color="black"/>
                              </TouchableOpacity>
                          </View>
                          <View style={styles.productOptionDetails}>
                              <TouchableOpacity style={styles.detailOptionOne}>
                                  <FontAwesome name="trash-o" size={21} color="white"/>
                              </TouchableOpacity>
                              <TouchableOpacity style={styles.detailOptionTwo} onPress={(item)=>this.addToCart(productInfo)}>
                                    <Feather name="shopping-cart" color="white" size={21} />
                              </TouchableOpacity>
                          </View>
                      </View>
                  </View>
              </View>
              <View style={styles.productsMoreDetails}>
                  <View style={styles.MoredetailOption}>
                      <Text style={styles.productDetailHeader}>Description</Text>
                      <Text style={styles.productDetailHeaderInfo}>Soft, mild-tasting fresh cheese from milk and cream</Text>
                  </View>
                  <View style={styles.MoredetailOption}>
                        <TouchableOpacity style={styles.collapseHeader} onPress={()=>this.setState({collapsed:!this.state.collapsed})}>
                            <Text style={styles.collapseHeaderText}>Ingreditents</Text>
                            {this.state.collapsed?(<MaterialIcons name="keyboard-arrow-up" color="white" size={26}/>):(<MaterialIcons name="keyboard-arrow-right" color="white" size={26}/>)}
                        </TouchableOpacity>
                        <Collapse 
                          isCollapsed={this.state.collapsed} 
                          onToggle={(isCollapsed)=>this.setState({collapsed:isCollapsed})}>
                          <CollapseHeader>
                            {/*<Text style={{color:'white'}}>Click here</Text>*/}
                          </CollapseHeader>
                          <CollapseBody>
                            <View style={styles.collapseBody}>
                                <Text style={styles.collapseBodyText}>
                                    &#8226; Carbonated water.{'\n'}{'\n'}
                                    &#8226;Sugar (sucrose or high-fructose corn syrup (HFCS) depending on country of origin){'\n'}{'\n'}
                                    &#8226; Caffeine. {'\n'}{'\n'}
                                    &#8226; Phosphoric acid. {'\n'}{'\n'}
                                    &#8226; Caramel color (E150d). {'\n'}{'\n'}
                                    &#8226; Natural flavorings. {'\n'}{'\n'}
                                </Text>
                            </View>
                          </CollapseBody>
                        </Collapse>
                  </View>

              </View>
              <View style={styles.relatedProductsDetail}>
                  <View style={styles.relatedproductDetailHeader}>
                    <Text style={styles.productDetailHeader}>Relate Products</Text> 
                    <Badge
                      value="10+"
                      // status="success"
                      badgeStyle={{backgroundColor:'#232149'}}
                      containerStyle={{position: 'absolute', top: -4, right: 20 }}
                    />
                  </View>
                  <View style={styles.relatedProductView}>
                      <FlatList
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        data={this.state.relatedProducts}
                        renderItem={({ item: rowData }) => {
                            return (  
                              <TouchableOpacity style={[styles.relatedproductitemContainer, { backgroundColor: 'white' }]}>
                                  <View style={styles.productImageSection}>
                                    <Image
                                      source={rowData.productImage}
                                      style={styles.productImage}
                                      PlaceholderContent={<ActivityIndicator />}
                                    />
                                  </View>
                                  <View style={styles.productInfomation}>
                                    <View style={styles.leftProductInfo}>
                                      <Text style={styles.itemName}>Kiwi Fruits</Text>
                                          <Text style={styles.itemCode}>Fresho</Text>
                                          <View style={styles.bottomSection}>
                                            <Text style={styles.itemPrice}>RWF 500</Text>
                                            <TouchableOpacity style={styles.addcartButton}>
                                              <Text style={styles.cartbutton}>
                                                <Feather name="shopping-cart" color="white" size={21.5} />
                                              </Text>
                                            </TouchableOpacity>
                                          </View>
                                    </View>
                                    {/*<View style={styles.rightProductInfo}>
                                      <View>
                                        <Text style={styles.itemPrice}>RWF 500</Text>
                                      </View>
                                    </View>*/}
                                        
                                  </View>
                              </TouchableOpacity>
                            );
                        }}
                        keyExtractor={(item, index) => index.toString()}
                      />
                  </View>    
              </View>

          </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
      flex: 1,
      // paddingTop: 15,
      backgroundColor: '#181532',
      // backgroundColor: '#f4f3f8',
  },
  mainHeader:{
      width:'100%',
      flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center',
      padding:10,
      // backgroundColor:'white',
  },
  pageHeader:{
      flexDirection:'row',
      marginTop:0,
      paddingHorizontal:15,
      // justifyContent:'space-between',
  },
  pageHeaderTitle:{
      color:'white',
      fontSize:25,
      fontFamily:Fonts.MontSerratSemiBold,

  },
  usernameInputContainer:{
      marginLeft:'5%',
      justifyContent:'center',
  },
  addmore:{
      fontFamily:Fonts.MontSerratSemiBold,
      color:'#531de3',
  },
  productcard:{

      backgroundColor:'#ffffff',
      borderBottomRightRadius:30,
      borderBottomLeftRadius:30,
      // marginLeft:10,
      width:'100%',
      // height:350,
      // borderRadius:10,
  },
  productcardImageSection:{
      width:'90%',
      flexDirection:'row',
      justifyContent:'space-between',
      padding:10,
  },
  productimageContainer:{
      width:'100%',
      alignItems:'center',
      borderRadius:10,
      // backgroundColor:'red',
  },
  productClassifications:{
      paddingHorizontal:'5%',
  },
  subcategorie:{
    backgroundColor:'#181532',
    // padding:5,
    color:'white',
    width:'auto',
    alignSelf:'flex-start',
    paddingVertical:3,
    paddingHorizontal:6,
    fontFamily:Fonts.MontSerrat,
    borderRadius:5,
    // width:'200%'
  },
  categorie:{
    marginTop:'1%',
    fontFamily:Fonts.MontSerrat,
    fontSize:12,
    color:'gray',
  },
  productName:{
    fontFamily:Fonts.MontSerratSemiBold,
    fontSize:18,

  },
  cartInfo:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center',
    paddingVertical:6,
  },
  productOptionDetails:{
    flexDirection:'row',
    justifyContent:'space-around',
  },
  productNumber:{
    backgroundColor:'#181532',
    flexDirection:'row',
    paddingHorizontal:2,
    paddingVertical:2,
    borderRadius:20,
    justifyContent:'space-between',
    alignItems:'center',
    // width:'30%',
  },
  quantityReduce:{
    backgroundColor:'white',
    borderRadius:20,
    padding:5,
    left:0,

  },
  quantityReached:{
    color:'white',
    paddingHorizontal:10,
    fontFamily:Fonts.MontSerratSemiBold,
    fontSize:18,
  },
  quantityIncrease:{
    backgroundColor:'white',
    borderRadius:20,
    padding:5,
  },
  stockStatus:{
    color:'#cacaca',
    fontFamily:Fonts.MontSerrat,
  },
  detailOptionOne:{
    backgroundColor:'#181532',
    padding:9,
    borderWidth:0.9,
    // borderColor:'#cacaca',
    borderColor:'#181532',
    borderRadius:50,
  },
  detailOptionTwo:{
    backgroundColor:'#181532',
    padding:9,
    borderWidth:0.9,
    borderColor:'#181532',
    borderRadius:50,
    marginLeft:5,

  },
  productsMoreDetails:{
      // backgroundColor:'#f4f3f8',
      backgroundColor:'#181532',
      // alignItems:'center',
      marginTop:'5%',
      // marginBottom:300,
      paddingHorizontal:'5%',
  },
  relatedProductsDetail:{
      // backgroundColor:'#f4f3f8',
      backgroundColor:'#181532',
      marginTop:'5%',
      marginBottom:100,
  },
  MoredetailOption:{
      paddingBottom:'4%',
      paddingTop:'4%',
      borderBottomWidth:1,
      borderBottomColor:'#c5c5c6',
  },

  productDetailHeader:{
      fontFamily:Fonts.MontSerratSemiBold,
      color:'white',
      fontSize:15,
  },
  relatedproductDetailHeader:{
      fontFamily:Fonts.MontSerratSemiBold,
      color:'white',
      fontSize:15,
      paddingHorizontal:'5%',
  },
  productDetailHeaderInfo:{
    // color:'#cdcdcd',
    color:'#c5c5c6',
    fontFamily:Fonts.MontSerrat,
    fontSize:13,
    marginTop:1,
  },
  collapseHeader:{
    flexDirection:'row',
    justifyContent:'space-between',
  },
  collapseHeaderText:{
    fontFamily:Fonts.MontSerratSemiBold,
    color:'white',
    fontSize:15,
  },
  collapseBody:{

  },
  collapseBodyText:{
      fontFamily:Fonts.MontSerrat,
      color:'white',
  },
  //related Products Styles
  relatedProductView:{

  },
  relatedproductitemContainer: {
      marginLeft:10,
      marginTop:10,
      justifyContent: 'flex-end',
      borderRadius: 5,
      padding: 3,
      height: 250,
      width:200,
  },
  productInfomation:{
    flexDirection:'row',
    // backgroundColor:'brown',
    justifyContent:'space-between',
  },

  productImageSection:{
    width:'100%',
    height:'74%',
    borderRadius:10,
    // height:'68%',
    // backgroundColor:'red',
    // borderRadius:10,
  },
  productImage:{
    width:'100%',
    height:'100%',
    borderRadius:10,
  },

  leftProductInfo:{
    width:'100%',
  },
  itemName: {
    fontFamily:Fonts.MontSerratSemiBold,
    fontSize: 12,
    color: '#1d1c1e',
    fontWeight: '600',
  },
  itemCode: {
    fontFamily:Fonts.MontSerrat,
    fontSize: 10,
      color: '#b8bebf',
  },
  itemPrice:{
    fontSize:12,
    fontFamily:Fonts.MontSerratSemiBold,
    color:'#181532',
  },
  bottomSection:{
    width:'100%',
    // backgroundColor:'red',
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center',
  },
  addcartButton:{
    backgroundColor:'#181532',
    borderTopLeftRadius:10,
    borderBottomRightRadius:5,
    marginRight:'-1.65%',
    marginBottom:'-1.65%',
    // position:'absolute',
    // right:0,
  },
  cartbutton:{
    color:'white',
    padding:5,

  },
  rightProductInfo:{
    fontSize:18,
    fontFamily:Fonts.MontSerratSemiBold,
  },
  //To Delete
   addCardForm:{
      // backgroundColor:'green',
      width:'80%',
      height:100,
  },
  cardInfo:{
    flexDirection:'row'
  },

  addPaymentButton:{
      borderRadius:50,
      paddingHorizontal:'32%',
      paddingVertical:'5%',
      fontFamily:Fonts.MontSerrat,
      backgroundColor:'#160e4e',
      borderWidth:1.5,
      borderColor:'white',
  },
  addPaymentText:{
      color:'white',
      fontFamily:Fonts.MontSerratSemiBold,
      fontSize:15,
  },
 
});
