
import React,{Component} from 'react';
import {StyleSheet, ScrollView, Text, View, Image, ActivityIndicator, SafeAreaView, TouchableOpacity,TouchableHighlight} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Ionicons from 'react-native-vector-icons/Ionicons'; 
import Icon from 'react-native-vector-icons/MaterialIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'; 
import AppIntroSlider from 'react-native-app-intro-slider';
import {iOSUIKit, systemWeights, sanFranciscoWeights, robotoWeights,human,material } from 'react-native-typography';
import { Input,Button  } from 'react-native-elements';
// import {LinearGradient} from 'expo-linear-gradient';
import LinearGradient from 'react-native-linear-gradient';
import Css from "../scripts/Design";
import {Fonts} from "../scripts/Fonts";
export default class Login extends Component{
	render(){
		return(
			<ScrollView style={styles.scrollView}>
				<View style={styles.mainContainer}>
					<View style={styles.loginSection}>
	            			<Image source={require('../assets/Baby-Mobile.png')} style={styles.loginImage}/>
	            			<Text style={styles.loginHeaderText}>Let's Get To Know You More</Text>
	            			<Text style={styles.loginText}>Please your information to secure your'e orders more</Text>
	            			<View style={styles.personalDetails}>
		            			<Input
								  	placeholder='First Name'
								  	leftIcon={
								    	<Entypo
								      		name='v-card'
								      		size={24}
								      		color='white'
								    	/>
								  	}
								  	inputContainerStyle={styles.phoneInput}
								  	inputStyle={{fontFamily:Fonts.MontSerrat,color:'white',fontSize:13}}
								  	placeholderTextColor="white"
								/>
								<Input
								  	placeholder='Last Name'
								  	leftIcon={
								    	<Entypo
								      		name='v-card'
								      		size={24}
								      		color='white'
								    	/>
								  	}
								  	inputContainerStyle={styles.phoneInput}
								  	inputStyle={{fontFamily:Fonts.MontSerrat,color:'white',fontSize:13,}}
								  	placeholderTextColor="white"
								/>
								<Input
								  	placeholder='Address'
								  	leftIcon={
								    	<Entypo
								      		name='address'
								      		size={24}
								      		color='white'
								    	/>
								  	}
								  	inputContainerStyle={styles.phoneInput}
								  	inputStyle={{fontFamily:Fonts.MontSerrat,color:'white',fontSize:13,}}
								  	placeholderTextColor="white"
								/>
								<Input
								  	placeholder='Email'
								  	leftIcon={
								    	<MaterialCommunityIcons
								      		name='email-variant'
								      		size={24}
								      		color='white'
								    	/>
								  	}
								  	inputContainerStyle={styles.phoneInput}
								  	inputStyle={{fontFamily:Fonts.MontSerrat,color:'white',fontSize:13,}}
								  	placeholderTextColor="white"
								/>
							</View>
							<Button
	  							icon={{name: 'arrow-right', type: 'font-awesome' ,color:'white',size:15,style: styles.authenticationInnerIcon}}
							  	// raised
							  	title="Continue"
							  	containerStyle={{height:'9.5%',color:'red',marginBottom:'5%'}}
								iconContainerStyle={{position: 'absolute', right: 10}}
							  	TouchableComponent={TouchableOpacity}
							  	ViewComponent={LinearGradient} // Don't forget this!
							  	linearGradientProps={{
								    colors: ['#160e4e', '#556af9'],
								    start: { x: 0, y: 0.5 },
								    end: { x: 1, y: 0.5 },
								}}
							  	buttonStyle={styles.authenticationButton}
							  	textStyle={{fontFamily: Fonts.MontSerrat}}
							  	titleStyle={{fontFamily: Fonts.MontSerrat}}
								onPress={()=>this.props.navigation.navigate('homeScreen',null)}
							/>

					</View>
				</View>
			</ScrollView>
		)
	}
}
const styles = StyleSheet.create({
	scrollView:{
        flex: 1,
        height: "100%",
        backgroundColor: '#181532',
	},
	mainContainer:{
        flex: 1,
        backgroundColor: '#181532',
        opacity: 1,
		alignItems:'center',

	},
	loginSection:{
		// backgroundColor:'red',
		marginTop:'5%',
		width:'90%',
		height:'100%',
		alignItems:'center',
		justifyContent:'center',

	},
	loginImage:{
		width:100,
		height:100,
	},
	loginHeaderText:{
		marginTop:'3%',
		fontFamily:Fonts.MontSerratSemiBold,
		fontSize:17,
		textAlign:'center',
		color:'white',
	},
	loginText:{
		marginTop:'1%',
		fontFamily:Fonts.MontSerrat,
		fontSize:15,
		textAlign:'center',
		color:'white',
	},
	personalDetails:{
		marginTop:'20%',
	},
	phoneInput:{
		marginTop:'-7%',
		paddingHorizontal:10,
		// borderColor:'#d9d9d6',
		borderColor:'#232149',
		backgroundColor:'#232149',
		borderWidth:1,
		borderRadius:5,
		width:'90%',
		marginLeft:'5%',
		color:'white',

	},
	authenticationButtonContainer:{
		// backgroundColor:,
		height:'50%',
	},
	authenticationButton:{
		borderRadius:25,
		paddingHorizontal:'32%',
		paddingVertical:'5%',
		fontFamily:Fonts.MontSerrat,
		// backgroundColor:'#160e4e',
	},
	authenticationInnerIcon:{
		backgroundColor:'#3954bb',
		padding:4,
		borderRadius:40,
	}
})