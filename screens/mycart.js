import React,{Component} from 'react';
import {StyleSheet, ScrollView, Text, View, ActivityIndicator,ProgressBarAndroid, SafeAreaView, TouchableOpacity,TouchableHighlight, FlatList} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Ionicons from 'react-native-vector-icons/Ionicons'; 
import Icon from 'react-native-vector-icons/MaterialIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import Feather from 'react-native-vector-icons/Feather';
import Fontisto from 'react-native-vector-icons/Fontisto';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'; 
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'; 
import { Input,Button,Header,Avatar,withBadge,Badge,Image   } from 'react-native-elements';
// import LinearGradient from 'react-native-linear-gradient';
import {LinearGradient} from 'expo-linear-gradient';
import DropDownPicker from 'react-native-dropdown-picker';
import Css from "../scripts/Design";
import {Fonts} from "../scripts/Fonts";

export default class mycart extends Component{
  constructor(props) {
      super(props);
      this.state = {
          username: 'Thierry',
          relatedProducts:[
            { productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/coca.jpg'), productId: 1,productQuantity:1,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
            { productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/cocacola.jpg'), productId: 2,productQuantity:1,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
            { productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/banana.jpg'), productId: 3,productQuantity:1,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
            { productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/cocacola.jpg'), productId: 4,productQuantity:1,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
            { productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/coca.jpg'), productId: 5,productQuantity:1,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
            { productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/banana.jpg'), productId: 6,productQuantity:1,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
            { productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/cocacola.jpg'), productId: 7,productQuantity:1,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
            { productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/banana.jpg'), productId: 8,productQuantity:1,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
          ],
          cartDataOld:[
            { productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/coca.jpg'), productId: 1,productQuantity:1,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
            { productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/cocacola.jpg'), productId: 2,productQuantity:1,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
            { productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/banana.jpg'), productId: 3,productQuantity:1,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
            { productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/cocacola.jpg'), productId: 4,productQuantity:1,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
            { productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/coca.jpg'), productId: 5,productQuantity:1,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
            { productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/banana.jpg'), productId: 6,productQuantity:1,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
            { productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/cocacola.jpg'), productId: 7,productQuantity:1,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
            { productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/banana.jpg'), productId: 8,productQuantity:1,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
          ],
          cart:null,
          quantityNeeded:1,
          loading:true,
      };
      this.cart_total=0;
  }

  componentDidMount() {
    AsyncStorage.getItem('@myCart').then((user_data_json) => {
        let cartData = JSON.parse(user_data_json);
        // alert(JSON.stringify(cartData));
        if(cartData != null) {
          // this.state.cart.push(cartData);
          this.setState({cart:cartData});
          this.setState({loading:false});
        }else{
          // alert('Object is null');
        }
    });
  }
  formatNumberWithCommas(x) {
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g , ",");
      // return x.replace(/B(?=(d{3})+(?!d))/g, ",");
  }
  changeProductCartQuantity(productId,event,index){
    let quantityNeeded = this.state.cart[index].productQuantity;
    if (event == 'increase') {
      this.state.cart[index].productQuantity=quantityNeeded+1;
      this.setState({cart:this.state.cart})
    }else{
      this.state.cart[index].productQuantity=quantityNeeded-1;
      this.setState({cart:this.state.cart})

    }
    this.cart_total=0;
  }
  addToCart(item){
    // alert(JSON.stringify(this.state.cart));
    if (this.state.cart.length > 0) {
      let Index=this.state.cart.findIndex(obj => obj.productId === item.productId);
      // alert(Index);
      if(Index < 0){
        this.state.cart.push(item);
        let myCart=JSON.stringify(this.state.cart);
        // alert(myCart);
        try {
          AsyncStorage.setItem('@myCart', myCart);
        } catch(e) {}
        alert('Product Has Been Added To Cart');
        this.setState({cart:this.state.cart});
        
      }else{
        // alert('The Product Exists in Cart')
      }
    }
    else{
      // let product=[item];
      // this.setState({cart:product});
      // this.setState({loading:false});
      // let myCart=JSON.stringify(product);
      // try {
      //   AsyncStorage.setItem('@myCart', myCart);
      // } catch(e) {}
      // alert('Product Has Been Added To Cart')
        
        let product=[item];
        this.state.cart.push(item);
        let myCart=JSON.stringify(product);
        try {
          AsyncStorage.setItem('@myCart', myCart);
        } catch(e) {}
        alert('Product Has Been Added To Cart')
        this.setState({cart:this.state.cart});
        this.setState({loading:false});
    }
    
  }
  deleteproductFromCart(productId,event,index){
    this.state.cart.splice(index,1);
    this.setState({cart:this.state.cart});
    AsyncStorage.setItem('@myCart', JSON.stringify(this.state.cart));
  }

  cartRelatedproductView(item){
      const productInfo={
        productName: item.productName,
        productCategorie:item.productCategorie,
        productImage:item.productImage,
        productId: item.productId,
        productStockQuantity:item.productQuantity,
        productQuantity:1,
        productPrice:item.productPrice,
        productDescription:item.productDescription,
      }
      return(
        <TouchableOpacity style={[styles.relatedproductitemContainer, { backgroundColor: 'white' }]} onPress={() => this.props.navigation.navigate('productDetail',productInfo)}>
            <View style={styles.productImageSection}>
              <Image
                source={item.productImage}
                style={styles.productImage}
                PlaceholderContent={<ActivityIndicator />}
              />
            </View> 
            <View style={styles.productInfomation}>
              <View style={styles.leftProductInfo}>
                <Text style={styles.itemName}>Kiwi Fruits</Text>
                    <Text style={styles.itemCode}>Fresho</Text>
                    <View style={styles.bottomSection}>
                      <Text style={styles.itemPrice}>RWF 500</Text>
                      <TouchableOpacity style={styles.addcartButton} onPress={(item)=>this.addToCart(productInfo)}>
                        <Text style={styles.cartbutton}>
                          {/*<Fontisto name="arrow-right-l" color="white" size={21.5} />*/}
                          <Feather name="shopping-cart" color="white" size={21.5} />
                        </Text>
                      </TouchableOpacity>
                    </View>
              </View>
              {/*<View style={styles.rightProductInfo}>
                <View>
                  <Text style={styles.itemPrice}>RWF 500</Text>
                </View>
              </View>*/}
                  
            </View>
        </TouchableOpacity>
      )
  }

  render() {
    const GradientBtn = ({ name }) => (
       <LinearGradient colors={['#160e4e', '#556af9']} start={{ y: 0.5, x: 0 }} end={{ x: 1, y: 0.5 }}  style={styles.orderNowButton}>
          <Text style={styles.orderNowButtonText}>Order Now</Text>
          <FontAwesome name="long-arrow-right" color="#181532" size={21.5} />
       </LinearGradient>
    )
    const { state, goBack } = this.props.navigation;
    return (
      <View style={styles.container}>
          <View style={styles.settingmainHeader}>
              <TouchableOpacity style={styles.backIcon} onPress={() => this.props.navigation.goBack(null)}>
                  <MaterialIcons name="keyboard-arrow-left" size={30} color="white"/>
              </TouchableOpacity>
              <View>
                  <Text style={{fontSize: 15,fontFamily:Fonts.MontSerratSemiBold,color:'white'}}>My Bag</Text>
              </View>
              {/*<TouchableOpacity>
                  <Ionicons name="md-settings" color="white" size={16}/>
              </TouchableOpacity>*/}
          </View>
          <ScrollView style={styles.cartScrollView}>
              <View style ={styles.pageHeader}>
                <Text style ={styles.pageHeaderTitle}>
                    My Cart
                </Text>
                <TouchableOpacity style = {styles.usernameInputContainer}>
                    {/*<Text style={styles.addmore}>ADD MORE +</Text>*/}
                </TouchableOpacity>
              </View>

              <View style={{height:15}}></View>
              <Text style ={styles.pageSubHeaderTitle}>Cart Related Products </Text>
              <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={this.state.relatedProducts}
                renderItem={({ item }) => this.cartRelatedproductView(item)}
                keyExtractor={(item, index) => index.toString()}
              />

              <Text style ={styles.pageSubHeaderTitle}>My Cart Products </Text>
              <View style={styles.allCartSection}>
                    
                    {
                      this.state.loading!=true && this.state.cart ?(
                        this.state.cart.map((item,index)=>{
                            this.cart_total=this.cart_total+(item.productPrice*item.productQuantity);
                            return(
                                <View style={styles.cartData}> 
                                    <View style={styles.cartProductImageSection}>
                                      <Image
                                          source={require('../assets/products/coca.jpg')}
                                          style={styles.cartproductImage}
                                          PlaceholderContent={<ActivityIndicator />}
                                      />
                                    </View>
                                    <View style={styles.cartProductCartDescription}>
                                        <View style={styles.cartProductCartDescriptionContent}>
                                          <Text style={styles.cartproductName} numberOfLines={1}>Texture Trench Coat</Text>
                                          <Text style={styles.cartproductDescr} numberOfLines={1}>Fresh Drinks</Text>
                                          <Text style={styles.cartproductPrice} numberOfLines={1}>RWF {item.productPrice}</Text>
                                        </View>
                                        <View>
                                          <TouchableOpacity style={styles.deleteproductCart} onPress={()=>this.deleteproductFromCart(item.productId,'delete',index)}>                           
                                            <FontAwesome5 name="trash-alt" color="white" size={14} />
                                            <Text style={{fontFamily:Fonts.MontSerrat,color:'white',fontSize:10}}> Delete</Text>
                                          </TouchableOpacity>
                                        </View>
                                        <View style={styles.cartProductQuantity}>
                                          <TouchableOpacity style={styles.cartproductquantityReduce} onPress={()=>this.changeProductCartQuantity(item.productId,'decrease',index)}>
                                            <Entypo name="minus" size={21} color="black"/>
                                          </TouchableOpacity>
                                          <Text style={styles.cartproductquantityReached}>{item.productQuantity}</Text>
                                          <TouchableOpacity style={styles.cartproductquantityIncrease} onPress={()=>this.changeProductCartQuantity(item.productId,'increase',index)}>
                                            <Entypo name="plus" size={21} color="black"/>
                                          </TouchableOpacity> 
                                      </View>
                                    </View>
                                </View>
                            )
                        })
                      ):
                      (
                        this.state.cart?(<ProgressBarAndroid color="white" size="small"/>):(<Text style={styles.noCartText}>No Cart Available</Text>)
                      )
                    }

                    
                    
                  
              </View>

          </ScrollView>
          <View style={styles.orderView}>
            <View style={styles.orderViewDescription}>
                <Text style={styles.cartTotal}>Total</Text>
                <Text style={styles.cartTotalPrice}>RWF {this.formatNumberWithCommas(this.cart_total)}</Text>
            </View>
            <View style={styles.orderNowButtonSection}>
              <TouchableOpacity  onPress={() => this.props.navigation.navigate('delivery',null)}>
                  {/*<GradientBtn name="Login" />
                  <Text style={styles.orderNowButtonText}>Order Now</Text>
                  <FontAwesome name="long-arrow-right" color="#181532" size={21.5} />*/}
                  <LinearGradient colors={['#160e4e', '#556af9']} start={{ y: 0.5, x: 0 }} end={{ x: 1, y: 0.5 }}  style={styles.orderNowButton}>
                      <Text style={styles.orderNowButtonText}>Order Now</Text>
                      <FontAwesome name="long-arrow-right" color="white" size={21.5} />
                  </LinearGradient>
              </TouchableOpacity>
            </View>
          </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
      flex: 1,
      // paddingTop: 15,
      // backgroundColor: '#efeff4',
      backgroundColor: '#181532',
  },
  settingmainHeader:{
      width:'100%',
      flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center',
      padding:10,
      // backgroundColor:'#232148',
  },
  pageHeader:{
      flexDirection:'row',
      marginTop:0,
      paddingHorizontal:15,
      // justifyContent:'space-between',
  },
  pageHeaderTitle:{
      color:'white',
      fontSize:25,
      fontFamily:Fonts.MontSerratSemiBold,

  },
  cartScrollView:{
      flex:1,
  },
  pageSubHeaderTitle:{
      paddingVertical:'2%',
      color:'white',
      fontSize:15,
      color:'#eaeef0',
      textAlign: 'right',
      fontFamily:Fonts.MontSerratSemiBold,
      // backgroundColor:'red',
  },
  usernameInputContainer:{
      marginLeft:'5%',
      justifyContent:'center',
  },
  addmore:{
      fontFamily:Fonts.MontSerratSemiBold,
      color:'#531de3',
  },
  relatedproductitemContainer: {
      marginLeft:10,
      marginTop:10,
      justifyContent: 'flex-end',
      borderRadius: 5,
      padding: 3,
      height: 250,
      width:200,
  },
  productInfomation:{
    flexDirection:'row',
    // backgroundColor:'brown',
    justifyContent:'space-between',
  },

  productImageSection:{
    width:'100%',
    height:'74%',
    borderRadius:10,
    // height:'68%',
    // backgroundColor:'red',
    // borderRadius:10,
  },
  productImage:{
    width:'100%',
    height:'100%',
    borderRadius:10,
  },

  leftProductInfo:{
    width:'100%',
  },
  itemName: {
    fontFamily:Fonts.MontSerratSemiBold,
    fontSize: 12,
    color: '#1d1c1e',
    fontWeight: '600',
  },
  itemCode: {
    fontFamily:Fonts.MontSerrat,
    fontSize: 10,
      color: '#b8bebf',
  },
  itemPrice:{
    fontSize:12,
    fontFamily:Fonts.MontSerratSemiBold,
    color:'#181532',
  },
  bottomSection:{
    width:'100%',
    // backgroundColor:'red',
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center',
  },
  addcartButton:{
    backgroundColor:'#181532',
    borderTopLeftRadius:10,
    borderBottomRightRadius:5,
    marginRight:'-1.65%',
    marginBottom:'-1.65%',
    // position:'absolute',
    // right:0,
  },
  cartbutton:{
    color:'white',
    padding:5,

  },
  rightProductInfo:{
    fontSize:18,
    fontFamily:Fonts.MontSerratSemiBold,
  },
  allCartSection:{
      alignItems:'center',
      // marginTop:'5%',
      marginBottom:100,
  },
  noCartText:{
    fontFamily:Fonts.MontSerratSemiBold,
    color:'white',
    marginTop:'10%',
    fontSize:20,
    textAlign:'center',
  },
  cartData:{
    flexDirection:'row',
    width:'95%',
    height:100,
    backgroundColor:'white',
    borderRadius:10,
    marginTop:'2.5%',
    // justifyContent:'space-between',
  },
  cartProductImageSection:{
    width:'30%',
    height:'100%',
    // alignItems:'center',
    // backgroundColor:'red',
    borderRadius:10,
  },
  cartproductImage:{
    width:'90%',
    height:'100%',
    borderRadius:10,
    // borderWidth:1,
    // borderColor:'#181532',
    // backgroundColor:'blue',
  },
  cartProductCartDescription:{
    // backgroundColor:'red',
    width:'70%',
    // paddingHorizontal:'1.5%',
    flexDirection:'row',
    justifyContent:'space-between',
    borderRadius:10,
  },
  cartproductName:{
    fontFamily:Fonts.MontSerrat,
    color:'black',
  },
  cartproductDescr:{
    marginTop:'2%',
    fontFamily:Fonts.MontSerrat,
    color:'#c2cacd',
  },
  cartproductPrice:{
    marginTop:'23%',
    fontFamily:Fonts.MontSerratSemiBold,
    color:'black',

  },
  cartProductQuantity:{
    // backgroundColor:'#181532',
    // flexDirection:'row',
    paddingHorizontal:2,
    paddingVertical:2,
    borderRadius:20,
    justifyContent:'space-between',
    alignItems:'center',
    // width:'30%',
  },
  deleteproductCart:{
    flexDirection:'row',
    backgroundColor:'#181532',
    borderRadius:5,
    marginTop:'2%',
    paddingHorizontal:5,
    paddingVertical:2,
  },
  cartproductquantityReduce:{
    backgroundColor:'white',
    borderRadius:20,
    padding:5,
    left:0,

  },
  cartproductquantityReached:{
    // color:'white',
    color:'black',
    paddingHorizontal:10,
    fontFamily:Fonts.MontSerratSemiBold,
    fontSize:18,
  },
  cartproductquantityIncrease:{
    backgroundColor:'white',
    borderRadius:20,
    padding:5,
  },

  orderView:{
    // height:60,
    backgroundColor:'#181532',
    // backgroundColor:'white',
    paddingVertical:'3%',
  },
  orderViewDescription:{
      flexDirection:'row',
      justifyContent:'space-between',
      paddingHorizontal:'4%',
      // paddingVertical:'2%',
      marginBottom:'4%',
  },
  cartTotal:{
      fontFamily:Fonts.MontSerrat,
      color:'#c2cacd',
      fontSize:14,
  },
  cartTotalPrice:{
      fontFamily:Fonts.MontSerratSemiBold,
      color:'white',
      fontSize:14,

  },
  orderNowButtonSection:{
      paddingHorizontal:'4.5%',
  },
  orderNowButton:{
      backgroundColor:'white',
      // backgroundColor:'#232149',
      padding:10,
      borderRadius:50,
      flexDirection:'row',
      justifyContent:'space-between',
  },
  orderNowButtonText:{
      color:'white',
      // color:'#181532',
      fontFamily:Fonts.MontSerratSemiBold,
  },
});
