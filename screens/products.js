import React,{Component} from 'react';
import {StyleSheet, ScrollView, Text, View, ActivityIndicator, SafeAreaView, TouchableOpacity,TouchableHighlight, FlatList} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Ionicons from 'react-native-vector-icons/Ionicons'; 
import Icon from 'react-native-vector-icons/MaterialIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'; 
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'; 
import Feather from 'react-native-vector-icons/Feather';
import {iOSUIKit, systemWeights, sanFranciscoWeights, robotoWeights,human,material } from 'react-native-typography';
import { Input,Button,Header,Avatar,withBadge,Badge,Image,SearchBar } from 'react-native-elements';
import { FlatGrid,SectionGrid } from 'react-native-super-grid';
import LinearGradient from 'react-native-linear-gradient';
// import {LinearGradient} from 'expo-linear-gradient';
import Css from "../scripts/Design";
import {Fonts} from "../scripts/Fonts";
export default class products extends Component{
	constructor(props) {
      super(props);
      this.state = {
        isLoading: true, 
        categorieData: [
        	{
        		image:require('../assets/categorie1.png'),
        		name:'Jarred\n Goods',
        	},
        	{
        		image:require('../assets/categorie4.png'),
        		name:'Cleaners',
        	},
        	{
        		image:require('../assets/categorie3.png'),
        		name:'Bread/\nBakery',
        	},
        	{
        		image:require('../assets/categorie2.png'),
        		name:'Bakery',
        	},
        	{
        		image:require('../assets/categorie5.png'),
        		name:'Cleaners',
        	},
        	{
        		image:require('../assets/categorie1.png'),
        		name:'Jarred\n Goods',
        	},
        	{
        		image:require('../assets/categorie4.png'),
        		name:'Personal\n Care',
        	},
        	{
        		image:require('../assets/categorie3.png'),
        		name:'Bread/\nBakery',
        	},
        	{
        		image:require('../assets/categorie2.png'),
        		name:'Bakery',
        	},
        	{
        		image:require('../assets/categorie5.png'),
        		name:'Cleaners',
        	},
        ],
        productsData:[
        	{ productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/coca.jpg'), productId: 1,productQuantity:20,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
        	{ productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/cocacola.jpg'), productId: 2,productQuantity:20,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
        	{ productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/banana.jpg'), productId: 3,productQuantity:20,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
        	{ productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/cocacola.jpg'), productId: 4,productQuantity:20,productPrice:1500,productDescription:'Fresh Juice with an expection proteins' },
        	{ productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/coca.jpg'), productId: 1,productQuantity:20,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
        	{ productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/banana.jpg'), productId: 1,productQuantity:20,productPrice:2500,productDescription:'Fresh Juice with an expection proteins' },
        	{ productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/cocacola.jpg'), productId: 1,productQuantity:20,productPrice:2000,productDescription:'Fresh Juice with an expection proteins' },
        	{ productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/mango.jpg'), productId: 1,productQuantity:20,productPrice:1200,productDescription:'Fresh Juice with an expection proteins' },
        	{ productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/chocolate3.jpg'), productId: 1,productQuantity:20,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
        	{ productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/juice1.jpg'), productId: 1,productQuantity:20,productPrice:1800,productDescription:'Fresh Juice with an expection proteins' },
        	{ productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/buscuit5.jpg'), productId: 1,productQuantity:20,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
        	{ productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/Agashya.jpg'), productId: 1,productQuantity:20,productPrice:4500,productDescription:'Fresh Juice with an expection proteins' },
        	{ productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/chocolate2.jpg'), productId: 1,productQuantity:20,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
        	{ productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/Buscuit2.jpg'), productId: 1,productQuantity:20,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
        ],
        adverts:[
        	{
        		advertTitle:'Hundred & One nutrition products',
        		advertSubTitle:'You can buy great products without having doughts',
        		advertImage:require('../assets/advert/advert6.png'),
        		advertBackground:'#ff6977',
        		advertProductId:1,
        	},
        	{
        		advertTitle:'Hundred & One nutrition products',
        		advertSubTitle:'You can buy great products without having doughts',
        		advertImage:require('../assets/advert/advert1.png'),
        		advertBackground:'#04b984',
        		advertProductId:1,
        	},
        	{
        		advertTitle:'Hundred & One nutrition products',
        		advertSubTitle:'You can buy great products without having doughts',
        		advertImage:require('../assets/advert/advert2.png'),
        		advertBackground:'#ffd75e',
        		advertProductId:3,
        	},
        ],
        cart:[],
        search: '',
        sliderIndex:0,
        maxSlider:1,
      }
    }
    scrollToIndex = (index, animated) => {
	   this.flatList1 && this.flatList1.scrollToIndex({ index, animated })
	}
    componentDidMount() {
    	this.setState({maxSlider:this.state.adverts.length-1})
    	// AsyncStorage.removeItem('@myCart');
    	// AsyncStorage.removeItem('@myWishList');
      	AsyncStorage.getItem('@myCart').then((user_data_json) => {
      		// alert(user_data_json);
          	let cartData = JSON.parse(user_data_json);
          	if(cartData != null) {
          		// this.state.cart.push(cartData);
          		this.setState({cart:cartData});
          	}else{
          		// alert('Object is null');
          	}
      	});


      	setInterval(function() {
		     const { sliderIndex, maxSlider } = this.state
		     let nextIndex = 0

		     if (sliderIndex < maxSlider) {
		       nextIndex = sliderIndex + 1
		     }

		     this.scrollToIndex(nextIndex, true)
		     this.setState({sliderIndex: nextIndex})
	   	}.bind(this), 3000)
    }
    updateSearch = (search) => {
	    this.setState({ search });
		};
    formatNumberWithCommas(x) {
	    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g , ",");
	    // return x.replace(/B(?=(d{3})+(?!d))/g, ",");
	}
    addToCart(item){
    	// alert(JSON.stringify(this.state.cart));
   		if (this.state.cart.length > 0) {
   			let Index=this.state.cart.findIndex(obj => obj.productId === item.productId);
   			// alert(Index);
	    	if(Index < 0){
	    		this.state.cart.push(item);
	    		let myCart=JSON.stringify(this.state.cart);
	    		// alert(myCart);
	    		try {
	    			AsyncStorage.setItem('@myCart', myCart);
	    		} catch(e) {}
	    		alert('Product Has Been Added To Cart')
				  
	    	}else{
	    		// alert('The Product Exists in Cart')
	    	}
   		}
   		else{ 
   			let product=[item];
   			this.state.cart.push(item);
    		let myCart=JSON.stringify(product);
    		try {
    			AsyncStorage.setItem('@myCart', myCart);
    		} catch(e) {}
	    	alert('Product Has Been Added To Cart')
   		}
    	
    }
    checkActive(index){
    	let updateIndex=index;
    	if (updateIndex == this.state.sliderIndex) {
    		return "circle";
    	}else{
    		return "circle-outline";	
    	}
    }
    productView(item){
    	const productInfo={
    		productName: item.productName,
    		productCategorie:item.productCategorie,
    		productImage:item.productImage,
    		productId: item.productId,
    		productStockQuantity:item.productQuantity,
    		productQuantity:1,
    		productPrice:item.productPrice,
    		productDescription:item.productDescription,
    	}
    	return(
    		<TouchableOpacity style={[styles.productitemContainer, { backgroundColor: 'white' }]} onPress={() => this.props.navigation.navigate('productDetail',productInfo)}>
    			<View style={styles.productImageSection}>
    				<Image
					 	source={item.productImage}
					  	style={styles.productImage}
					  	PlaceholderContent={<ActivityIndicator />}
					/>
    			</View>
    			<View style={styles.productInfomation}>
    				<View style={styles.leftProductInfo}>
	    				<Text style={styles.itemName}>Kiwi Fruits</Text>
		          		<Text style={styles.itemCode}>Fresho</Text>
		          		<View style={styles.bottomSection}>
		          			<Text style={styles.itemPrice}>RWF {this.formatNumberWithCommas(item.productPrice)}</Text>
		          			<TouchableOpacity style={styles.addcartButton} onPress={(item)=>this.addToCart(productInfo)}>
		          				<Text style={styles.cartbutton}>
		          					<Feather name="shopping-cart" color="white" size={21.5} />
		          				</Text>
		          			</TouchableOpacity>
		          		</View>
    				</View>	          		
	          	</View>
	        </TouchableOpacity>
    	)
    }
    advertView(item){
    	const advertInfo={
    		advertTitle:item.advertTitle,
    		advertSubTitle:item.advertSubTitle,
    		advertImage:item.advertImage,
    		advertBackground:item.advertBackground,
    		advertProductId:item.advertProductId,
    	}
    	return (	
      		<View style={styles.advertismentView}>
				<View style={styles.leftAdvertView}>
					<Text style={styles.advertTitle}>Hundred & One nutrition products</Text>
					<Text style={styles.advertSubTitle}>You can buy great products without having doughts</Text>
					{/*<Text style={styles.advertSubTitle}>You can draw good pictures without learning sketches</Text>*/}
				</View>
				<View style={styles.rightAdvertView}>
					<Image
					 	source={item.advertImage}
					  	style={{width:'100%',height:'100%',borderRadius:10}}
					  	PlaceholderContent={<ActivityIndicator />}
					/>
				</View>
			</View>
      	);
    }
	render(){
		const { search } = this.state;
		return(
			<ScrollView style={styles.scrollView}>
				<View style={styles.productsmainContainer}>
					<View style={styles.mainHeader}>
						<TouchableOpacity style={styles.backIcon}>
							<MaterialIcons name="keyboard-arrow-left" size={30} color="white"/>
						</TouchableOpacity>
						<SearchBar
					        placeholder="Type Here..."
					        onChangeText={this.updateSearch}
					        value={search}
					        searchIcon={{color:'white'}}
					        clearIcon={{color:'white'}}
					        cancelIcon={{color:'white'}}
					        platform="android"
					        containerStyle={{width:'85%',height:'90%',alignItems:'center',marginRight:'3%',backgroundColor:'none'}}
					        inputContainerStyle={{backgroundColor:'none',borderRadius:5,borderBottomWidth:1,borderBottomColor:'white'}}
					        inputStyle={{color:'white'}}
					        placeholderTextColor='gray'
					        leftIconContainerStyle={{color:'white'}}
					        // showLoading={true}
					    />
					</View>
					<View style={styles.productCategories}>
						<ScrollView >
							<FlatList
					            horizontal
					            showsHorizontalScrollIndicator={false}
					            initialScrollIndex='3'
					            data={this.state.categorieData}
					            renderItem={({ item: rowData }) => {
					              	return (	
					              		<TouchableOpacity style={styles.categorie}>
						              		<Image
											 	source={rowData.image}
											  	style={{ width: 60, height: 60,borderRadius:60,borderWidth:1,borderColor:'white',}}
											  	PlaceholderContent={<ActivityIndicator />}
											/>
											<View><Text style={styles.categorieName}>{rowData.name}</Text></View>
											<Badge
											    status="success"
											    containerStyle={{ position: 'absolute', top: 5, right:3 }}
											/>
										</TouchableOpacity>
					              	);
					            }}
					            keyExtractor={(item, index) => index.toString()}
				          	/>
						</ScrollView>
					</View>
					<View style={styles.advertismentViewSection}>
						<FlatList
							ref={flatList1 => { this.flatList1 = flatList1 }}
				            horizontal
				            showsHorizontalScrollIndicator={false}
				            data={this.state.adverts}
					      	renderItem={({ item }) => this.advertView(item)}
				            keyExtractor={(item, index) => index.toString()}
			          	/>
						<View style={styles.advertNavigationButtons}>
							{
								this.state.adverts.map((item,index)=>{
									return(
										<MaterialCommunityIcons name={this.checkActive(index)} size={13} color="lightgreen"/>
									)
								})
							}
                              {/*<MaterialCommunityIcons name="circle-outline" size={13} color="lightgreen"/>
                              <MaterialCommunityIcons name="circle" size={13} color="lightgreen"/>*/}
                              
						</View>
					</View>
					<View>
						<FlatGrid
					      itemDimension={130}
					      data={this.state.productsData}
					      style={styles.gridView}
					      // staticDimension={300}
					      fixed={false}
					      horizontal={false}
					      // spacing={10}
					      renderItem={({ item }) => this.productView(item)}
					    />
					</View>
				</View>
			</ScrollView>
		)
	}
}
const styles = StyleSheet.create({
	scrollView:{
        flex: 1,
        height: "100%",
        backgroundColor: '#181532',
        paddingBottom:200,
	},
	productsmainContainer:{
        flex: 1,
        backgroundColor: '#181532',
        opacity: 1,

	},
	mainHeader:{
		width:'100%',
		flexDirection:'row',
		justifyContent:'space-between',
		alignItems:'center',
	},
	backIcon:{
		padding:10,
	},
	productCategories:{
		flexDirection:'row',
	},
	advertismentViewSection:{
		marginTop:'4%',
		// backgroundColor:'red',
		width:'100%',
		height:180,
		alignItems:'center',
		justifyContent:'center',
	},
	advertismentView:{
		// backgroundColor:'#04b984',
		backgroundColor:'#ff6977',
		// backgroundColor:'#ffd75e',
		// width:'90%',
		// height:'100%',
		width:330,
		height:'100%',
		marginLeft:15,
		// marginRight:2,
		borderRadius:10,
		flexDirection:'row',
		padding:6,
	},
	advertNavigationButtons:{
		flexDirection:'row',
	},
	leftAdvertView:{
		width:'60%',
		height:'100%',
		// backgroundColor:'blue',
		borderRadius:10,
		alignItems:'center',
		// justifyContent:'center',
		paddingLeft:'1%',

	},
	advertTitle:{
		fontFamily:Fonts.MontSerratSemiBold,
		fontSize:23,
		color:'white',
	},
	advertSubTitle:{
		fontFamily:Fonts.MontSerrat,
		fontSize:14,
		color:'white',

	},
	rightAdvertView:{
		width:'40%',
		height:'100%',
		// backgroundColor:'black',
		borderRadius:10,
	},
	categorie:{
		marginLeft:5,
	},
	categorieName:{
		color:'white',
		fontFamily:Fonts.MontSerratSemiBold,
		textAlign:'center',
		justifyContent:'center',
		alignItems:'center',
		// backgroundColor:'red',
		// width:'90%',
	},



	gridView: {
    	marginTop: 10,
    	flex: 1,
  	},
	productitemContainer: {
	    justifyContent: 'flex-end',
	    borderRadius: 5,
	    padding: 3,
	    height: 230,
	},
	productInfomation:{
		flexDirection:'row',
		// backgroundColor:'brown',
		justifyContent:'space-between',
	},

	productImageSection:{
		width:'100%',
		height:'74%',
		// height:'68%',
		// backgroundColor:'red',
		borderRadius:10,
		// alignItems:'center',
	},
	productImage:{
		width:'80%',
		height:'100%',
		marginLeft:'10%',
		borderRadius:10,
		// backgroundColor:'rgba(255, 255, 255, .5)'
	},

	leftProductInfo:{
		width:'100%',
	},
	itemName: {
		fontFamily:Fonts.MontSerratSemiBold,
		fontSize: 12,
	    color: '#1d1c1e',
	    fontWeight: '600',
	},
	itemCode: {
		fontFamily:Fonts.MontSerrat,
		fontSize: 10,
	    color: '#b8bebf',
	},
	itemPrice:{
		fontSize:12,
		fontFamily:Fonts.MontSerratSemiBold,
		color:'#181532',
	},
	bottomSection:{
		width:'100%',
		// backgroundColor:'red',
		flexDirection:'row',
		justifyContent:'space-between',
		alignItems:'center',
	},
	addcartButton:{
		backgroundColor:'#181532',
		borderTopLeftRadius:10,
		borderBottomRightRadius:5,
		marginRight:'-1.65%',
		marginBottom:'-1.65%',
		// position:'absolute',
		// right:0,
	},
	cartbutton:{
		color:'white',
		padding:5,

	},
	rightProductInfo:{
		fontSize:18,
		fontFamily:Fonts.MontSerratSemiBold,
	},
})