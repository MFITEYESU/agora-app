import React,{Component} from 'react';
import {StyleSheet, ScrollView, Text, View, ActivityIndicator, SafeAreaView, TouchableOpacity,TouchableHighlight, FlatList} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Ionicons from 'react-native-vector-icons/Ionicons'; 
import Icon from 'react-native-vector-icons/MaterialIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import Feather from 'react-native-vector-icons/Feather';
import Fontisto from 'react-native-vector-icons/Fontisto';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'; 
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'; 
import { Input,Button,Header,Avatar,withBadge,Badge,Image   } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
// import {LinearGradient} from 'expo-linear-gradient';
import DropDownPicker from 'react-native-dropdown-picker';
import Css from "../scripts/Design";
import {Fonts} from "../scripts/Fonts";

export default class mypayments extends Component{
  constructor(props) {
      super(props);
      this.state = {
          code: 'option-val-123',
          accounts:[],
          options:[],
          username: 'Thierry',
          paymentsData: [
              {
                name:'masterCard',
                cardImage:require('../assets/cards/visa2.png'),
                image:require('../assets/categorie1.png'),
                name:'Categorie',
              },
              {
                name:'masterCard',
                cardImage:require('../assets/cards/mastercard.png'),
                image:require('../assets/categorie4.png'),
                name:'Categorie',
              },
              {
                name:'masterCard',
                cardImage:require('../assets/cards/visa.png'),
                image:require('../assets/categorie3.png'),
                name:'Categorie',
              },
              {
                name:'masterCard',
                cardImage:require('../assets/cards/mastercard2.png'),
                image:require('../assets/categorie2.png'),
                name:'Categorie',
              },
          ],
          cardId:[
              {
                label: 'MASTER CARD', 
                value: 'MASTER CARD', 
                icon: () => <Fontisto name="mastercard" size={18} color="gold" />
              },
              {
                label: 'VISA CARD', 
                value: 'VISA CARD', 
                icon: () => <Fontisto name="visa" size={18} color="gold" />
              },
                              
          ],
          country: 'VISA CARD'
      };
  }

  render() {
    const { state, goBack } = this.props.navigation;
    return (
      <View style={styles.container}>
          <View style={styles.settingmainHeader}>
              <TouchableOpacity style={styles.backIcon} onPress={() => this.props.navigation.goBack(null)}>
                  <MaterialIcons name="keyboard-arrow-left" size={30} color="white"/>
              </TouchableOpacity>
              <View>
                  <Text style={{fontSize: 15,fontFamily:Fonts.MontSerratSemiBold,color:'white'}}>My payments</Text>
              </View>
              {/*<TouchableOpacity>
                  <Ionicons name="md-settings" color="white" size={16}/>
              </TouchableOpacity>*/}
          </View>
          <ScrollView>
              <View style ={styles.pageHeader}>
                
                <Text style ={styles.pageHeaderTitle}>
                    Cards
                </Text>
                <TouchableOpacity style = {styles.usernameInputContainer}>
                    <Text style={styles.addmore}>ADD MORE +</Text>
                </TouchableOpacity>
              </View>

              <View style={{height:50}}></View>
              <FlatList
                  horizontal
                  showsHorizontalScrollIndicator={false}
                  data={this.state.paymentsData}
                  renderItem={({ item: rowData }) => {
                      return (  
                        <TouchableOpacity style={styles.card}>
                          <View style={styles.cardTitle}>
                              <View>
                                  <Image
                                      source={rowData.cardImage}
                                      style={{ width: 50, height: 35,}}
                                      PlaceholderContent={<ActivityIndicator />}
                                  />
                              </View>
                              <View style={styles.cardTitleLeft}>  
                                  <AntDesign name="setting" color="gray" size={20}/>
                              </View>
                          </View>
                          <View style={styles.cardNumberSection}>
                            <Text style={styles.cardNumber}>&#8727;&#8727;&#8727;&#8727;  &#8727;&#8727;&#8727;&#8727;  &#8727;&#8727;&#8727;&#8727;  5600</Text>
                          </View>
                          <View style={styles.cardOwnerInfo}>
                              <Text style={styles.cardOwnerName}>JOHN SMITH</Text>
                              <View style={styles.cardDated}>
                                  <View style={styles.expLabel}>
                                    <Text style={styles.expLabelText}>GOOD</Text>
                                    <Text style={styles.expLabelText}>THRU</Text> 
                                  </View>
                                  <Text style={styles.expDate}>12/23</Text>
                              </View>
                          </View>
                          {/*<Image
                            source={rowData.image}
                              style={{ width: 60, height: 60,borderRadius:60,borderWidth:1,borderColor:'white'}}
                              PlaceholderContent={<ActivityIndicator />}
                          />*/}
                        </TouchableOpacity>
                      );
                  }}
                  keyExtractor={(item, index) => index.toString()}
              />
              <View style={styles.addCardSection}>
                  <View style={styles.addCardForm}>
                      <Input
                        label="CARD NUMBER"
                        containerStyle={{height:'70%'}}
                        labelStyle={{fontSize:10,fontFamily:Fonts.MontSerrat,color:'gold'}}
                        placeholder='Eg. 0000 0000 0000 0020'
                        inputStyle={{fontFamily:Fonts.MontSerrat,color:'white',fontSize:15}}
                        placeholderTextColor="white"
                        leftIcon={
                          <AntDesign
                            name='creditcard'
                            size={20}
                            color='gold'
                          />
                        }
                      />
                      <View style={styles.cardInfo}>
                          <Input
                            label="NAME"
                            containerStyle={{width:'50%',height:'70%'}}
                            labelStyle={{fontSize:10,fontFamily:Fonts.MontSerrat,color:'gold'}}
                            placeholder='Eg. Thierry'
                            inputStyle={{fontFamily:Fonts.MontSerrat,color:'white',fontSize:15}}
                            placeholderTextColor="white"
                            leftIcon={
                              <AntDesign
                                name='user'
                                size={24}
                                color='gold'
                              />
                            }
                          />
                          <DropDownPicker
                              items={this.state.cardId}
                              defaultValue={this.state.country}
                              containerStyle={{height: 40,width:'50%',marginTop:24,}}
                              style={{borderWidth:0,backgroundColor:'#181532',borderBottomWidth:1,borderBottomColor:'#b5b5b5',}}
                              itemStyle={{
                                  justifyContent: 'flex-start',
                                  color:'white',
                              }}
                              // arrowStyle={{backgroundColor:'gold'}}
                              activeItemStyle ={{paddingLeft:2.5,borderWidth:0,borderBottomWidth:1,borderColor:'gold'}}
                              dropDownStyle={{backgroundColor: '#181532',zIndex:1000}}
                              labelStyle={{fontSize: 13, textAlign: 'left',color: 'white',fontFamily:Fonts.MontSerrat}}
                              onChangeItem={item => this.setState({
                                  country: item.value
                              })}
                          />
                      </View>
                      <View style={styles.cardInfo}>
                          <Input
                            label="EXPIRATION DATE"
                            containerStyle={{width:'50%',height:'70%'}}
                            labelStyle={{fontSize:10,fontFamily:Fonts.MontSerrat,color:'gold'}}
                            placeholder='Eg. 05/22'
                            inputStyle={{fontFamily:Fonts.MontSerrat,color:'white',fontSize:15}}
                            placeholderTextColor="white"
                            leftIcon={
                              <Fontisto
                                name='date'
                                size={24}
                                color='gold'
                              />
                            }
                          />
                          <Input
                            label="CVV"
                            containerStyle={{width:'50%',height:'70%'}}
                            labelStyle={{fontSize:10,fontFamily:Fonts.MontSerrat,color:'gold'}}
                            placeholder='Eg. 325'
                            inputStyle={{fontFamily:Fonts.MontSerrat,color:'white',fontSize:15}}
                            placeholderTextColor="white"
                            leftIcon={
                              <MaterialCommunityIcons
                                name='key-outline'
                                size={24}
                                color='gold'
                              />
                            }
                          />
                      </View>
                      <TouchableOpacity style={styles.addPaymentButton}><Text style={styles.addPaymentText}>Add Payment</Text></TouchableOpacity>
                      
                  </View>
                  
              </View>

          </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
      flex: 1,
      // paddingTop: 15,
      // backgroundColor: '#efeff4',
      backgroundColor: '#181532',
  },
  settingmainHeader:{
      width:'100%',
      flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center',
      padding:10,
      // backgroundColor:'#232148',
  },
  pageHeader:{
      flexDirection:'row',
      marginTop:0,
      paddingHorizontal:15,
      // justifyContent:'space-between',
  },
  pageHeaderTitle:{
      color:'white',
      fontSize:25,
      fontFamily:Fonts.MontSerratSemiBold,

  },
  usernameInputContainer:{
      marginLeft:'5%',
      justifyContent:'center',
  },
  addmore:{
      fontFamily:Fonts.MontSerratSemiBold,
      color:'#531de3',
  },
  card:{
      backgroundColor:'#ffffff',
      marginLeft:10,
      width:300,
      height:180,
      borderRadius:10,
  },
  cardTitle:{
      flexDirection:'row',
      justifyContent:'space-between',
      padding:10,
  },
  cardTitleLeft:{
      alignItems:'center',
      justifyContent:'center',
  },
  cardNumberSection:{
      marginTop:'7%',
      marginBottom:'8%',
      alignItems:'center',
  },
  cardNumber:{
      fontFamily:Fonts.MontSerrat,
      color:'#323232',
      fontSize:28,
  },
  cardOwnerInfo:{
      flexDirection:'row',
      justifyContent:'space-between',
      paddingHorizontal:10,
  },
  cardOwnerName:{
      fontFamily:Fonts.MontSerratSemiBold,
      color:'#323232',
  },
  cardDated:{
      flexDirection:'row',
      justifyContent:'space-around',
  },
  expLabel:{
      marginRight:5,
  },
  expLabelText:{
      fontSize:7,
      fontFamily:Fonts.MontSerrat,
      color:'#cdced6',
  },
  expDate:{
      fontFamily:Fonts.MontSerratSemiBold,
      // fontSize:13,
  },
  addCardSection:{
      alignItems:'center',
      marginTop:'5%',
      marginBottom:300,
  },
  addCardForm:{
      // backgroundColor:'green',
      width:'80%',
      height:100,
  },
  cardInfo:{
    flexDirection:'row'
  },

  addPaymentButton:{
      borderRadius:50,
      paddingHorizontal:'32%',
      paddingVertical:'5%',
      fontFamily:Fonts.MontSerrat,
      backgroundColor:'#160e4e',
      borderWidth:1.5,
      borderColor:'white',
  },
  addPaymentText:{
      color:'white',
      fontFamily:Fonts.MontSerratSemiBold,
      fontSize:15,
  },
});
