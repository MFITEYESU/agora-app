import React,{Component} from 'react';
import {StyleSheet, ScrollView, Text, View, Image, ActivityIndicator, SafeAreaView} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Ionicons from 'react-native-vector-icons/Ionicons';  
import AppIntroSlider from 'react-native-app-intro-slider';
import {iOSUIKit, systemWeights, sanFranciscoWeights, robotoWeights,} from 'react-native-typography';
import Css from "../scripts/Design";

const slides = [
    {
        key: '1',
        title: 'Quickly and Easily',
        text: 'Just visit any of the growing number\n of outlet who accept money and\n make your purchase within second',
        image: require('../assets/intro/intro1.png'),
        backgroundColor: '#59b2ab',
    },
    {
        key: '2',
        title: 'Shopping Online',
        text: 'Just visit any of the growing number\n of outlet who accept money and\n make your purchase within second',
        image: require('../assets/intro/intro2.png'),
        backgroundColor: '#febe29',
    },
    {
        key: '3',
        title: 'Manage your fianance',
        text: 'Just visit any of the growing number\n of outlet who accept money and\n make your purchase within second',
        image: require('../assets/intro/intro3.png'),
        backgroundColor: '#22bcb5',
    },
    {
        key: '4',
        title: 'Evolve to digital World',
        text: 'Just visit any of the growing number\n of outlet who accept money and\n make your purchase within second',
        image: require('../assets/intro/intro4.png'),
        backgroundColor: '#22bcb5',
    },
];


export default class IntroSliderScreen extends Component {
    static navigationOptions = {
        header: null,
    };
    // const navigationOptions = {
    //    header: null,
    //    tabBarVisible: false,
    // }

    constructor(props) {
        super(props);
        this.state = {
    		showRealApp: false
        }
    }

    async componentDidMount() {
    }

    _renderItem = (item) => {
        return (
            <View style={Css.slide}>
            	<View style={Css.introImageView}>
            		<View style={Css.introImageSection}>
            			{<Image source={slides[item.index].image} style={Css.introImage}/>}
            		</View>
            	</View>
            	<View style={Css.introTextView}>
                	<Text style={Css.introTitle}>{slides[item.index].title}</Text>
                	<Text style={Css.introText}>{slides[item.index].text}</Text>
            	</View>
            </View>
        );
    };


    _onDone = () => {
        this.goLogin();
    };
    _renderNextButton = () => {
	    return (
	      <View style={Css.buttonCircle}>
	        <Ionicons
	          name="md-arrow-round-forward"
	          color="rgba(255, 255, 255, .9)"
	          size={24}
	          style={{ backgroundColor: 'transparent' }}
	        />
	      </View>
	    );
	 };
    _renderBackButton = () => {
        return(
            <View style={Css.backButton}>'
                <Text style={{color:'#09717b'}}>Back</Text>
            </View>
        )
    };
    _renderDoneButton = () => {
	    return (
	      <View style={Css.buttonCircle}>
	        <Ionicons
	          name="md-checkmark"
	          color="rgba(255, 255, 255, .9)"
	          size={24}
	          style={{ backgroundColor: 'transparent' }}
	        />
	      </View>
	    );
  	};
    _renderSkipButton = () => {
        return(<View style={Css.skipButton}><Text style={{color:'#09717b'}}>Skip</Text></View>)
    };

    goLogin(){
		this.props.navigation.navigate('loginScreen',null);
    }

    checkFirst() {
    }
    render() {
        return (
            <SafeAreaView style={{flex: 1,backgroundColor: 'white',}}>
            
                    <AppIntroSlider
                        renderItem={this._renderItem}
                        data={slides}
                        renderSkipButton={this._renderSkipButton}
                        renderDoneButton={this._renderDoneButton}
                        renderNextButton={this._renderNextButton}
                        renderBackButton={this._renderBackButton}
                        dotStyle={{backgroundColor:'#dcdcdd'/*,borderWidth:1,borderColor:'#29af8e',width:'1%'*/}}
                        activeDotStyle={{backgroundColor:'#531fdc'}}
                        onDone={this._onDone}
                    />
            </SafeAreaView>
        );
    }
}

