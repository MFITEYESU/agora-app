import React,{Component} from 'react';
import {StyleSheet, ScrollView, Text, View, ActivityIndicator, SafeAreaView, TouchableOpacity,TouchableHighlight, FlatList} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Ionicons from 'react-native-vector-icons/Ionicons'; 
import Icon from 'react-native-vector-icons/MaterialIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'; 
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'; 
import Feather from 'react-native-vector-icons/Feather';
import {iOSUIKit, systemWeights, sanFranciscoWeights, robotoWeights,human,material } from 'react-native-typography';
import { Input,Button,Header,Avatar,withBadge,Badge,Image   } from 'react-native-elements';
import { FlatGrid,SectionGrid } from 'react-native-super-grid';
import LinearGradient from 'react-native-linear-gradient';
// import {LinearGradient} from 'expo-linear-gradient';
import Css from "../scripts/Design";
import {Fonts} from "../scripts/Fonts";
export default class WishList extends Component{
	constructor(props) {
      super(props);
      this.state = {
        isLoading: true, 
        categorieData: [
        	{
        		image:require('../assets/categorie1.png'),
        		name:'Jarred\n Goods',
        	},
        	{
        		image:require('../assets/categorie4.png'),
        		name:'Cleaners',
        	},
        	{
        		image:require('../assets/categorie3.png'),
        		name:'Bread/\nBakery',
        	},
        	{
        		image:require('../assets/categorie2.png'),
        		name:'Bakery',
        	},
        	{
        		image:require('../assets/categorie5.png'),
        		name:'Cleaners',
        	},
        	{
        		image:require('../assets/categorie1.png'),
        		name:'Jarred\n Goods',
        	},
        	{
        		image:require('../assets/categorie4.png'),
        		name:'Personal\n Care',
        	},
        	{
        		image:require('../assets/categorie3.png'),
        		name:'Bread/\nBakery',
        	},
        	{
        		image:require('../assets/categorie2.png'),
        		name:'Bakery',
        	},
        	{
        		image:require('../assets/categorie5.png'),
        		name:'Cleaners',
        	},
        ],
        productsData:[
        	{ productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/coca.jpg'), productId: 1,productQuantity:20,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
        	{ productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/cocacola.jpg'), productId: 1,productQuantity:20,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
        	{ productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/banana.jpg'), productId: 1,productQuantity:20,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
        	{ productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/cocacola.jpg'), productId: 1,productQuantity:20,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
        	{ productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/coca.jpg'), productId: 1,productQuantity:20,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
        	{ productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/banana.jpg'), productId: 1,productQuantity:20,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
        	{ productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/cocacola.jpg'), productId: 1,productQuantity:20,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
        	{ productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/mango.jpg'), productId: 1,productQuantity:20,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
        	{ productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/chocolate3.jpg'), productId: 1,productQuantity:20,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
        	{ productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/juice1.jpg'), productId: 1,productQuantity:20,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
        	{ productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/buscuit5.jpg'), productId: 1,productQuantity:20,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
        	{ productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/Agashya.jpg'), productId: 1,productQuantity:20,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
        	{ productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/chocolate2.jpg'), productId: 1,productQuantity:20,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
        	{ productName: 'PRODUCT 1',productCategorie:'Bakery',productImage:require('../assets/products/Buscuit2.jpg'), productId: 1,productQuantity:20,productPrice:500,productDescription:'Fresh Juice with an expection proteins' },
        ],
        cart:[],
        wishList:[],
        loading:false,
      }
    }
  	componentDidMount(){
	    AsyncStorage.multiGet(['@myCart','@myWishList',]).then((user_data_json) => {
          	let cartData=user_data_json[0][1];
          	let wishlistData=user_data_json[1][1];
          	if (JSON.parse(cartData) != null) {
            	// this.state.cart.push(cartData);
              	this.setState({cart:JSON.parse(cartData)});
              	this.setState({loading:false});
          	}
          	if (JSON.parse(wishlistData) != null) {
              	// alert(wishlistData);
            	// this.state.cart.push(cartData);
              	this.setState({wishList:JSON.parse(wishlistData)});
              	this.setState({loading:false});
          	}
	    });
  	}
  	addToCart(item){
	    // alert(JSON.stringify(this.state.cart));
	    if (this.state.cart.length > 0) {
	      let Index=this.state.cart.findIndex(obj => obj.productId === item.productId);
	      // alert(Index);
	      if(Index < 0){
	        this.state.cart.push(item);
	        let myCart=JSON.stringify(this.state.cart);
	        // alert(myCart);
	        try {
	          AsyncStorage.setItem('@myCart', myCart);
	        } catch(e) {}
	        alert('Product Has Been Added To Cart')
	        
	      }else{
	        // alert('The Product Exists in Cart')
	      }
	    }
	    else{ 
	      let product=[item];
	      this.state.cart.push(item);
	      let myCart=JSON.stringify(product);
	      try {
	        AsyncStorage.setItem('@myCart', myCart);
	      } catch(e) {}
	      alert('Product Has Been Added To Cart')
	    }  
  	}


  	wishListRemover(productId,event,index){
    	this.state.wishList.splice(index,1);
    	this.setState({wishList:this.state.wishList});
    	AsyncStorage.setItem('@myWishList', JSON.stringify(this.state.wishList));
  	}

    WishListproductView(item){
    	const productInfo={
    		productName: item.productName,
    		productCategorie:item.productCategorie,
    		productImage:item.productImage,
    		productId: item.productId,
    		productStockQuantity:item.productQuantity,
    		productQuantity:1,
    		productPrice:item.productPrice,
    		productDescription:item.productDescription,
    	}
    	return(
    		<TouchableOpacity style={[styles.productitemContainer, { backgroundColor: 'white' }]} onPress={() => this.props.navigation.navigate('productDetail')}>
    			<View style={styles.WishproductImageSection}>
    				<Image
					 	source={require('../assets/products/mango.jpg')}
					  	style={styles.WishproductImage}
					  	PlaceholderContent={<ActivityIndicator />}
					/>
					<TouchableOpacity style={styles.WishproductDelete} onPress={()=>this.wishListRemover(item.productId,'delete',index)}>
						<FontAwesome5 name="trash-alt" color="white" size={16}/>
					</TouchableOpacity>
    			</View>
    			<View style={styles.productInfomation}>
    				<View style={styles.leftProductInfo}>
	    				<Text style={styles.itemName}>Kiwi Fruits</Text>
		          		<Text style={styles.itemCode}>Fresho</Text>
		          		<View style={styles.bottomSection}>
		          			<Text style={styles.itemPrice}>RWF 500</Text>
		          			<TouchableOpacity onPress={(item)=>this.addToCart(productInfo)}>
		          				<LinearGradient colors={['#160e4e', '#181532']} start={{ y: 0.9, x: 0.5 }} end={{ x: 1, y: 0.5 }}  style={styles.wishaddcartButton}>
		          				<Text style={styles.wishcartbutton}>
		          					ADD TO
		          					<Feather name="shopping-cart" color="white" size={14} />
		          				</Text>
		          				</LinearGradient>
		          			</TouchableOpacity>
		          		</View>
    				</View>	          		
	          	</View>
	        </TouchableOpacity>
    	)
    }
	render(){
		return(
			<ScrollView style={styles.scrollView}>
				<View style={styles.productsmainContainer}>
					<View style={styles.mainHeader}>
						<TouchableOpacity style={styles.backIcon}>
							<MaterialIcons name="keyboard-arrow-left" size={30} color="white"/>
						</TouchableOpacity>
						<Text style={{marginRight:'2%',fontFamily:Fonts.MontSerrat,color:'white'}}>
							WishList 
						</Text>
					</View>
					<View style={styles.productCategories}>
						<ScrollView >
							<View style ={styles.pageHeader}>
				                <Text style ={styles.pageHeaderTitle}>
				                    My Wishlist
				                </Text>
				                <TouchableOpacity style = {styles.usernameInputContainer}>
				                    {/*<Text style={styles.addmore}>ADD MORE +</Text>*/}
				                </TouchableOpacity>
				            </View>

				            <View style={{height:15}}></View>
						</ScrollView>
					</View>
					<View>
						<FlatGrid
					      itemDimension={130}
					      data={this.state.wishList}
					      style={styles.gridView}
					      // staticDimension={300}
					      fixed={false}
					      horizontal={false}
					      // spacing={10}
					      renderItem={({ item }) => this.WishListproductView(item)}
					    />
					</View>
				</View>
			</ScrollView>
		)
	}
}
const styles = StyleSheet.create({
	scrollView:{
        flex: 1,
        height: "100%",
        backgroundColor: '#181532',
        paddingBottom:200,
	},
	productsmainContainer:{
        flex: 1,
        backgroundColor: '#181532',
        opacity: 1,

	},
	mainHeader:{
		width:'100%',
		flexDirection:'row',
		justifyContent:'space-between',
		alignItems:'center',
	},
	pageHeader:{
      	flexDirection:'row',
	    marginTop:0,
	    paddingHorizontal:15,
	    // justifyContent:'space-between',
	},
	pageHeaderTitle:{
	    color:'white',
	    fontSize:25,
	    fontFamily:Fonts.MontSerratSemiBold,

	},
	backIcon:{
		padding:10,
	},
	productCategories:{
		flexDirection:'row',
	},
	categorie:{
		marginLeft:5,
	},
	categorieName:{
		color:'white',
		fontFamily:Fonts.MontSerratSemiBold,
		textAlign:'center',
		justifyContent:'center',
		alignItems:'center',
		// backgroundColor:'red',
		// width:'90%',
	},



	gridView: {
    	marginTop: 10,
    	flex: 1,
  	},
	productitemContainer: {
	    justifyContent: 'flex-end',
	    borderRadius: 5,
	    padding: 3,
	    height: 230,
	},
	productInfomation:{
		flexDirection:'row',
		// backgroundColor:'brown',
		justifyContent:'space-between',
	},

	WishproductImageSection:{
		width:'100%',
		height:'74%',
		// height:'68%',
		// backgroundColor:'red',
		borderRadius:10,
		// flexDirection:'row',
		// alignItems:'center',
	},
	WishproductImage:{
		width:'80%',
		height:'100%',
		// marginLeft:'10%',
		borderRadius:10,
		// backgroundColor:'rgba(255, 255, 255, .5)'
	},
	WishproductDelete:{
		position:'absolute',
		right:0,
		padding:6,
		backgroundColor:'#181532',
		borderRadius:15,
	},
	leftProductInfo:{
		width:'100%',
	},
	itemName: {
		fontFamily:Fonts.MontSerratSemiBold,
		fontSize: 12,
	    color: '#1d1c1e',
	    fontWeight: '600',
	},
	itemCode: {
		fontFamily:Fonts.MontSerrat,
		fontSize: 10,
	    color: '#b8bebf',
	},
	itemPrice:{
		fontSize:12,
		fontFamily:Fonts.MontSerratSemiBold,
		color:'#181532',
	},
	bottomSection:{
		width:'100%',
		// backgroundColor:'red',
		flexDirection:'row',
		justifyContent:'space-between',
		alignItems:'center',
	},
	wishaddcartButton:{
		backgroundColor:'#181532',
		padding:4,
		borderRadius:12,
		// borderTopLeftRadius:10,
		// borderBottomRightRadius:5,
		// marginRight:'-1.65%',
		// marginBottom:'-1.65%',
		marginRight:'0%',
		marginBottom:'0%',
		// position:'absolute',
		// right:0,
	},
	wishcartbutton:{
		color:'white',
		fontFamily:Fonts.MontSerratSemiBold,
		fontSize:11,
		// padding:3,

	},
	rightProductInfo:{
		fontSize:18,
		fontFamily:Fonts.MontSerratSemiBold,
	},
})