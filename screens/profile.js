import React,{Component} from 'react';
import {StyleSheet, ScrollView, Text, View, ActivityIndicator, SafeAreaView, TouchableOpacity,TouchableHighlight, FlatList} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Ionicons from 'react-native-vector-icons/Ionicons'; 
import Icon from 'react-native-vector-icons/MaterialIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'; 
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'; 
import { Input,Button,Header,Avatar,withBadge,Badge,Image   } from 'react-native-elements';
import Css from "../scripts/Design";
import {Fonts} from "../scripts/Fonts";

export default class SettingsScreen extends Component{
  constructor(props) {
      super(props);
      this.state = {
          code: 'option-val-123',
          accounts:[],
          options:[],
          username: 'Thierry' 
      };
  }

  render() {
    return (
      <View style={styles.container}>
          <View style={styles.settingmainHeader}>
              <View></View>
              <View>
                <Text style={{fontSize: 15,fontFamily:Fonts.MontSerratSemiBold,color:'white'}}>Settings</Text>
              </View>
              <TouchableOpacity>
                  <Ionicons name="md-settings" color="white" size={16}/>
              </TouchableOpacity>
          </View>
          <ScrollView>
              <View style ={styles.imgNameContainer}>
                <View style ={styles.avatarContainer}>
                  <Image
                      source={require('../assets/user.png')}
                      style={{ width: 48, height: 48,}}
                      PlaceholderContent={<ActivityIndicator />}
                  />
                </View>
                <View style = {styles.usernameInputContainer}>
                  <Text style={styles.username}>Ishimwe</Text>
                  <Text style={styles.editprofile}>Edit Profile</Text>
                </View>
              </View>

              <View style={{height:50}}></View>

              <TouchableOpacity  style = {styles.menuBtnContainer} onPress={() => this.props.navigation.navigate('myTransactions')}>
                  <View style={[styles.definedIcon,{backgroundColor:'green'}]}>
                      <Ionicons name="ios-swap" color="white" size={14}/>
                  </View>
                  <Text style = {styles.menuBtn}> My Transactions</Text>
                  <Ionicons style = {styles.menuRightIcon} name="ios-arrow-forward" size={20}/>
              </TouchableOpacity>
              <TouchableOpacity  style = {styles.menuBtnContainer} onPress={() => this.props.navigation.navigate('mypayments')}>
                  <View style={[styles.definedIcon,{backgroundColor:'green'}]}>
                      <Ionicons name="ios-options" color="white" size={14}/>
                  </View>
                  <Text style = {styles.menuBtn}> Payment Accounts</Text>
                  <Ionicons style = {styles.menuRightIcon} name="ios-arrow-forward" size={20}/>
              </TouchableOpacity>
              <TouchableOpacity  style = {styles.menuBtnContainer}>
                  <View style={[styles.definedIcon,{backgroundColor:'red'}]}>
                      <Ionicons name="ios-trash" color="white" size={14} style={{paddingHorizontal:2}}/>
                  </View>
                  <Text style = {styles.menuBtn}> Delete my account</Text>
                  <Ionicons style = {styles.menuRightIcon} name="ios-arrow-forward" size={20}/>
              </TouchableOpacity>
              <TouchableOpacity  style = {styles.menuBtnContainer}>
                  <View style={[styles.definedIcon,{backgroundColor:'red'}]}>
                      <Ionicons name="ios-log-out" color="white" size={14}/>
                  </View>
                  <Text style = {styles.menuBtn}> Sign out</Text>
                  <Ionicons style = {styles.menuRightIcon} name="ios-arrow-forward" size={20}/>
              </TouchableOpacity>

          </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // paddingTop: 15,
    // backgroundColor: '#efeff4',
    backgroundColor: '#181532',
  },
  settingmainHeader:{
    width:'100%',
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center',
    padding:10,
    // backgroundColor:'#232148',
  },
  imgNameContainer:{
    flexDirection:'row',
    marginTop:20,
    backgroundColor:'#232149',
    padding:15,
    // borderBottomWidth:0.3,
    // borderBottomColor:'#999',
  },
  avatarContainer:{
    width:70,
    height:70,
    borderRadius:60,
    borderWidth:1,
    borderColor:'white',
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'#181532',
  },
  usernameInputContainer:{
    marginLeft:'5%',
    justifyContent:'center',
  },
  username:{
    fontSize:22,
    color:'#c5c5c7',
    fontFamily:Fonts.MontSerrat,
  },
  editprofile:{
    fontSize:13,
    fontFamily:Fonts.MontSerrat,
    color:'white',
  },
  menuBtnContainer:{
    flexDirection:'row',
    padding:15,
    backgroundColor:'#232149',
    marginTop:0,
    borderBottomColor:'#181532',
    borderBottomWidth:1,
  },
  definedIcon:{
    borderRadius:5,
    paddingHorizontal:7,
    paddingVertical:6,
    backgroundColor:'black',
    marginRight:2,
  },
  menuBtn:{
    fontSize: 18,
    fontFamily:Fonts.MontSerrat,
    color:'white',
  },
  menuRightIcon:{
    position:'absolute',
    right:20,
    top:15,
    color:'#555'
  },
});
