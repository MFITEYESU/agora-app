import React,{Component,useState } from 'react';
import {Switch,StyleSheet, ScrollView, Text, View, ActivityIndicator, SafeAreaView, TouchableOpacity,TouchableHighlight, FlatList} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Ionicons from 'react-native-vector-icons/Ionicons'; 
import Icon from 'react-native-vector-icons/MaterialIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import Feather from 'react-native-vector-icons/Feather';
import Fontisto from 'react-native-vector-icons/Fontisto';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'; 
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'; 
import { Input,Button,Header,Avatar,withBadge,Badge,Image   } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
// import {LinearGradient} from 'expo-linear-gradient';
import DropDownPicker from 'react-native-dropdown-picker';
import Css from "../scripts/Design";
import {Fonts} from "../scripts/Fonts";

export default class palceOrder extends Component{
  constructor(props) {
      super(props);
      this.state = {
          code: 'option-val-123',
          accounts:[],
          options:[],
          username: 'Thierry',
          paymentsOptions: [
              {
                name:'Visa Card',
                cardImage:require('../assets/cards/visa2.png'),
                image:require('../assets/categorie1.png'),
                selected:false,
                cardType:1,
              },
              {
                name:'Master Card',
                cardImage:require('../assets/cards/mastercard.png'),
                image:require('../assets/categorie4.png'),
                selected:false,
                cardType:1,
              },
              {
                name:'MTN Mobile Money',
                cardImage:require('../assets/cards/momo.png'),
                image:require('../assets/categorie4.png'),
                selected:true,
                cardType:2,
              },
              {
                name:'Airtel',
                cardImage:require('../assets/cards/airtel.png'),
                image:require('../assets/categorie4.png'),
                selected:false,
                cardType:2,
              },
          ],
          selectedCard:2,
          cardId:[
              {
                label: 'MASTER CARD', 
                value: 'MASTER CARD', 
                icon: () => <Fontisto name="mastercard" size={18} color="gold" />
              },
              {
                label: 'VISA CARD', 
                value: 'VISA CARD', 
                icon: () => <Fontisto name="visa" size={18} color="gold" />
              },
                              
          ],
          country: 'VISA CARD',
          switch1Value: false,
      };
  }
  selectPayment(item,selectedInded){
      this.state.paymentsOptions[0].selected=false;
      this.state.paymentsOptions[1].selected=false;
      this.state.paymentsOptions[2].selected=false;
      this.state.paymentsOptions[3].selected=false;
      this.state.paymentsOptions[selectedInded].selected=true;
      this.setState({paymentsOptions:this.state.paymentsOptions});
      this.setState({selectedCard:this.state.paymentsOptions[selectedInded].cardType});

  }
  toggleSwitch1 = (value) => {
      this.setState({switch1Value: value})
      // console.log('Switch 1 is: ' + value)
  }
  render() {
    const { state, goBack } = this.props.navigation;
    return (
      <View style={styles.container}>
          <View style={styles.settingmainHeader}>
              <TouchableOpacity style={styles.backIcon} onPress={() => this.props.navigation.goBack(null)}>
                  <MaterialIcons name="keyboard-arrow-left" size={30} color="white"/>
              </TouchableOpacity>
              <View>
                  <Text style={{fontSize: 15,fontFamily:Fonts.MontSerratSemiBold,color:'white'}}>Payment data</Text>
              </View>
              {/*<TouchableOpacity>
                  <Ionicons name="md-settings" color="white" size={16}/>
              </TouchableOpacity>*/}
          </View>
          <ScrollView>
              <View style ={styles.paymentTotal}>
                <Text style ={styles.paymentTotalTitle}>Total Price</Text>
                <Text style ={styles.paymentTotalSubTitle}>
                    RWF 35,000
                </Text>
              </View>

              <View style={{height:15}}></View>
              <Text style ={styles.paymentMethodTitle}>Payment Methods</Text>
              <FlatList
                  horizontal
                  showsHorizontalScrollIndicator={false}
                  data={this.state.paymentsOptions}
                  renderItem={({ item: rowData,index:index }) => {
                      return (  
                        <TouchableOpacity style={[styles.paymentCard,{backgroundColor:rowData.selected?('orange'):('white')}]} onPress={(item,selectedInded)=>this.selectPayment(rowData,index)}>
                          <View style={styles.cardTitle}>
                              <View>
                                  <Image
                                      source={rowData.cardImage}
                                      style={{ width: 65, height: 40,borderRadius:10,}}
                                      PlaceholderContent={<ActivityIndicator />}
                                  />
                              </View>
                              <View style={styles.cardTitleLeft}>  
                                  <Ionicons name="md-checkmark-circle-outline" color="#181532" size={30}/>
                              </View>
                          </View>
                        </TouchableOpacity>
                      );
                  }}
                  keyExtractor={(item, index) => index.toString()}
              />
              {
                this.state.selectedCard == 1?(
                  <View style={styles.addCardSection}>
                      <View style={styles.addCardForm}>
                          <Input
                            label="CARD NUMBER"
                            containerStyle={{height:'70%'}}
                            labelStyle={{fontSize:10,fontFamily:Fonts.monospace,color:'gold'}}
                            placeholder='Eg. 0000 0000 0000 0020'
                            inputStyle={{fontFamily:Fonts.MontSerrat,color:'white',fontSize:15}}
                            placeholderTextColor="white"
                            leftIcon={
                              <AntDesign
                                name='creditcard'
                                size={20}
                                color='gold'
                              />
                            }
                          />

                          <View style={styles.cardInfo}>
                              <Input
                                label="EXPIRATION DATE"
                                containerStyle={{width:'50%',height:'70%'}}
                                labelStyle={{fontSize:10,fontFamily:Fonts.MontSerrat,color:'gold'}}
                                placeholder='Eg. 05/22'
                                inputStyle={{fontFamily:Fonts.MontSerrat,color:'white',fontSize:15}}
                                placeholderTextColor="white"
                                leftIcon={
                                  <Fontisto
                                    name='date'
                                    size={24}
                                    color='gold'
                                  />
                                }
                              />
                              <Input
                                label="CVV"
                                containerStyle={{width:'50%',height:'70%'}}
                                labelStyle={{fontSize:10,fontFamily:Fonts.MontSerrat,color:'gold'}}
                                placeholder='Eg. 325'
                                inputStyle={{fontFamily:Fonts.MontSerrat,color:'white',fontSize:15}}
                                placeholderTextColor="white"
                                leftIcon={
                                  <MaterialCommunityIcons
                                    name='key-outline'
                                    size={24}
                                    color='gold'
                                  />
                                }
                              />
                          </View>
                          <View style={styles.cardInfo}>
                              <Input
                                label="CARD HOLDER"
                                containerStyle={{width:'100%',height:'70%'}}
                                labelStyle={{fontSize:10,fontFamily:Fonts.MontSerrat,color:'gold'}}
                                placeholder='Eg. Thierry'
                                inputStyle={{fontFamily:Fonts.MontSerrat,color:'white',fontSize:15}}
                                placeholderTextColor="white"
                                leftIcon={
                                  <AntDesign
                                    name='user'
                                    size={24}
                                    color='gold'
                                  />
                                }
                              />
                          </View>
                          <View style={styles.saveCardSection}>
                              <Text style={styles.saveCard}>Save card data for future payments</Text>
                              <Switch
                                trackColor={{ false: "#767577", true: "#81b0ff" }}
                                styles={styles.switchView}
                                onValueChange = {this.toggleSwitch1}
                                value = {this.state.switch1Value}
                              />
                          </View>
                          <TouchableOpacity style={styles.confirmPayment}><Text style={styles.addPaymentText}>Proceed to confirm</Text></TouchableOpacity>
                          
                      </View>
                  </View>
                ):(
                  <View style={styles.addCardSection}>
                    <View style={styles.addCardForm}>
                        <Input
                          label="PHONE NUMBER"
                          containerStyle={{height:'70%'}}
                          labelStyle={{fontSize:10,fontFamily:Fonts.MontSerrat,color:'gold'}}
                          placeholder='Eg. 0783366893'
                          inputStyle={{fontFamily:Fonts.MontSerrat,color:'white',fontSize:15}}
                          placeholderTextColor="white"
                          leftIcon={
                            <AntDesign
                              name='creditcard'
                              size={20}
                              color='gold'
                            />
                          }
                        />
                        <View style={styles.saveCardSection}>
                            <Text style={styles.saveCard}>Save phone number for future payments</Text>
                            <Switch
                              trackColor={{ false: "#767577", true: "#81b0ff" }}
                              styles={styles.switchView}
                              onValueChange = {this.toggleSwitch1}
                              value = {this.state.switch1Value}
                            />
                        </View>
                        <TouchableOpacity style={styles.confirmPayment}><Text style={styles.addPaymentText}>Proceed to confirm</Text></TouchableOpacity>
                        
                    </View>
                  </View>
                )
              }

          </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
      flex: 1,
      // paddingTop: 15,
      // backgroundColor: '#efeff4',
      backgroundColor: '#181532',
  },
  settingmainHeader:{
      width:'100%',
      flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center',
      padding:10,
      // backgroundColor:'#232148',
  },
  paymentTotal:{
      // flexDirection:'row',
      marginTop:'4%',
      paddingHorizontal:15,
      // justifyContent:'space-between',
  },
  paymentTotalTitle:{
      color:'white',
      fontSize:15,
      fontFamily:Fonts.MontSerrat,

  },
  paymentMethodTitle:{
      color:'white',
      fontSize:15,
      fontFamily:Fonts.MontSerrat,
      paddingHorizontal:15,
      marginBottom:'1%',

  },
  paymentTotalSubTitle:{

      color:'white',
      fontSize:27,
      fontFamily:Fonts.MontSerratSemiBold,
  },
  usernameInputContainer:{
      marginLeft:'5%',
      justifyContent:'center',
  },
  addmore:{
      fontFamily:Fonts.MontSerratSemiBold,
      color:'#531de3',
  },
  paymentCard:{
      backgroundColor:'#ffffff',
      marginLeft:10,
      borderRadius:10,
      paddingVertical:3,
      paddingHorizontal:3,
      width:115,
  },
  cardTitle:{
      flexDirection:'row',
      justifyContent:'space-between',
      // padding:10,
  },
  cardTitleLeft:{
      alignItems:'center',
      justifyContent:'center',
  },
  addCardSection:{
      alignItems:'center',
      marginTop:'5%',
      marginBottom:300,
  },
  addCardForm:{
      // backgroundColor:'green',
      width:'80%',
      height:100,
  },
  cardInfo:{
    flexDirection:'row'
  },

  confirmPayment:{
      borderRadius:50,
      paddingHorizontal:'25%',
      paddingVertical:'5%',
      fontFamily:Fonts.MontSerrat,
      backgroundColor:'#160e4e',
      borderWidth:1.5,
      borderColor:'white',
  },
  addPaymentText:{
      color:'white',
      fontFamily:Fonts.MontSerratSemiBold,
      fontSize:15,
  },
  saveCardSection:{
    flexDirection:'row',
    marginLeft:-10,
    // backgroundColor:'red',
  },
  saveCard:{
      marginTop:15,
      marginRight:'4%',
      marginBottom:20,
      color:'white',
      fontFamily:Fonts.MontSerratSemiBold,
  },
  switchView:{
    marginTop:-5,
    backgroundColor:'blue',
  }
});
