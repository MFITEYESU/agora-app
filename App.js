import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,
  Animated,
  TouchableOpacity,
  Dimensions,
  ImageBackground,
  TextInput,
  Image,
  PixelRatio,
  PanResponder,
  ScrollView,
  TouchableHighlight,
  Alert,
  Modal,
  ProgressBarAndroid,
  ButtonGroup,
  NetInfo ,
  ActivityIndicator
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
// import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
// import { AppLoading, Asset, Font, Icon } from 'expo';
import { CheckBox,Badge} from 'react-native-elements';
import { iOSUIKit,systemWeights,sanFranciscoWeights,robotoWeights} from 'react-native-typography'
import introslider from './screens/introslider';
import loginScreen from './screens/loginScreen';
import verification from './screens/verification';
import userInfo from './screens/userInfo';
import homeScreen from './screens/homeScreen';
import mypayments from './screens/mypayments';
import productDetail from './screens/productDetail';
import myTransactions from './screens/myTransactions';
import placeOrder from './screens/placeOrder';
import delivery from './screens/delivery';
import trackOrder from './screens/trackOrder';
export default class App extends Component{
  render(){
    return(
        <NavigationContainer>
            <Stack.Navigator initialRouteName="introslider" headerMode="none">
                <Stack.Screen name="introslider" component={introslider}
                  options={{
                    navigationOptions: ({navigation, route }) => ({
                      headerShown: false
                    })
                  }}
                />

                <Stack.Screen name="loginScreen" component={loginScreen}
                  options={{
                    navigationOptions: ({navigation, route }) => ({
                      headerShown: false
                    })
                  }}
                />
                <Stack.Screen name="verification" component={verification}
                  options={{
                    navigationOptions: ({navigation, route }) => ({
                      headerShown: false
                    })
                  }}
                />
                <Stack.Screen name="userInfo" component={userInfo}
                  options={{
                    navigationOptions: ({navigation, route }) => ({
                      headerShown: false
                    })
                  }}
                />
                <Stack.Screen name="homeScreen" component={homeScreen}
                  options={{
                    navigationOptions: ({navigation, route }) => ({
                      headerShown: false
                    })
                  }}
                />
                <Stack.Screen name="mypayments" component={mypayments}
                  options={{
                    navigationOptions: ({navigation, route }) => ({
                      headerShown: false
                    })
                  }}
                />
                <Stack.Screen name="productDetail" component={productDetail}
                  options={{
                    navigationOptions: ({navigation, route }) => ({
                      headerShown: false
                    })
                  }}
                />
                <Stack.Screen name="myTransactions" component={myTransactions}
                  options={{
                    navigationOptions: ({navigation, route }) => ({
                      headerShown: false
                    })
                  }}
                />
                <Stack.Screen name="delivery" component={delivery}
                  options={{
                    navigationOptions: ({navigation, route }) => ({
                      headerShown: false
                    })
                  }}
                />
                <Stack.Screen name="placeOrder" component={placeOrder}
                  options={{
                    navigationOptions: ({navigation, route }) => ({
                      headerShown: false
                    })
                  }}
                />
                <Stack.Screen name="trackOrder" component={trackOrder}
                  options={{
                    navigationOptions: ({navigation, route }) => ({
                      headerShown: false
                    })
                  }}
                />
            </Stack.Navigator>
        </NavigationContainer>
    )
  }
}
const Stack=createStackNavigator();
