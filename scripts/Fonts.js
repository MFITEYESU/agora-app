export const Fonts = {
	MontSerrat:'Montserrat-Regular',
	MontSerratBold:'Montserrat-Bold',
	MontSerratSemiBold:'Montserrat-SemiBold',
}