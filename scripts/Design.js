import React from 'react';
import {Fonts} from "../scripts/Fonts";
import {Button, Platform, ScrollView, View, StyleSheet} from 'react-native';


const Css = StyleSheet.create({
    //IntroSLider 

    mainContainer: {
        flex: 1,
        backgroundColor: '#ffffff',
        opacity: 1
    },
    slide: {
        flex: 1,
        alignItems: 'center',
        // justifyContent: 'center',
        // backgroundColor:'black',
    },
    introImageView:{
        width:'100%',
        height:'65%',
        // backgroundColor:'#3c3c48',
        backgroundColor:'#050110',
        alignItems:'center',
        justifyContent:'center',
    },
    introImageSection:{
        // backgroundColor:'white',
        height:'100%',
        width:'100%',
        // marginTop:'20%',
    },
    introImage: {
        width: '100%',
        height: '100%',
        // marginBottom: 50
    },
    introTextView:{

        width:'100%',
        height:'35%',
        // backgroundColor:'#f7f7f7',
        backgroundColor:'#050110',
        justifyContent: 'center',
        alignItems: 'center',
    },

    introTitle: {
        textAlign: 'center',
        fontSize: 24,
        color: 'white',
        marginTop:15,
        fontWeight:'900',
        fontFamily:Fonts.MontSerratSemiBold,
    },
    introText: {
        marginBottom: 100,
        // padding: 50,
        paddingTop: 15,
        fontSize: 17,
        textAlign: 'center',
        // color: '#818181',
        lineHeight: 20,
        color:'#bababa',
        fontFamily:Fonts.MontSerrat,
    },
    buttonCircle: {
        width: 40,
        height: 40,
        backgroundColor: '#531fdc',
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    }, 
    nextButton:{
        paddingTop:12,
    },

    backButton:{
        paddingTop:12,
    },
    skipButton:{
        paddingTop:12,
    },
    doneButton:{
        paddingTop:12,
    }
    
});

export default Css;
